# A minha Rua

A Minha Rua é uma plataforma de participação pública, de interação entre as autoridades locais e o cidadão.

Sendo uma plataforma tão transparente aunto possível, o sódigo da mesma está também disponível para análise e para receber contribuições para a sua melhoria. 

## Participar como cidadão

Para participar na plataforma A Minha Rua, use o endereço [A Minha Rua](http://amr.osgeopt.pt)

## Participar como programador

Para participar como programador, siga as instruções que ainda estão por escrever.

## Dependências

Esta aplicação web, além do código neste repositório, depende de várias coisas:

* Servidor de mapas (está a ser utilizado o Geoserver + GeoWebCache)
* Biliotecas Javascript
    * ExtJS 4
    * OpenLayers 2.12
* Base de dados PostgreSQL + PostGIS

### Linguagens

São utilizadas as seguintes linguagens de programação:

* HTML
* CSS
* Javascript
* PHP
* SQL e PL/pgSQL

## Notas

Este documento está escrito no plsqlo [Markdown](http://en.wikipedia.com/wiki/Markdown).

## Desenvolver sobre Eclipse/Aptana

Estas breves notas ajudam a trabalhar no projeto *A Minha Rua*, utilizando o Eclipse ou o Aptana. O projeto está alojado no [Bitbucket](https://bitbucket.org).
Enquanto o projeto não está público, deverá pedir acesso ao repositório através dos emails do [Jorge](mailto:jgr@di.uminho.pt) ou do [Tozé](mailto:ajfsilva@gmail.com).

Convém, depois de [criar uma conta no Bitbucket](https://confluence.atlassian.com/display/BITBUCKET/Create+an+Account+and+a+Git+Repo), seguir os tutoriais e configurar adequadamente a [autenticação por chaves](https://confluence.atlassian.com/display/BITBUCKET/Set+up+SSH+for+Git).
 
É necessário ter o plugin EGit instalado no Eclipse/Aptana. Convém fazer o tutorial [Woking with EGit](http://www.vogella.com/articles/EGit/article.html) antes de avançar. 

#### Clonar o projecto (e guardar fora do workspace)

File -> Import -> Git Repository as New Project

URI: git@bitbucket.org:jgrocha/amr.git
Destination: ''uma pasta fora do workspace do Eclipse''

O novo projecto não pode ter um nome igual a outro projecto do Aptana, já existente.

##### Alterar ficheiros existentes

Os ficheiros que já existiam, podem ser alterados. Depois de efectuadas as alterações convenientes, basta fazer o commit dos mesmos. 
Os commits são contra o repositório local. 
No fim será necessário enviar as alterações para o repositório *origin*.

##### Criar novos ficheiros

Depois de criados so ficheiros localmente, qualquer que seja o tipo, mas que sejam necessários para o projeto, usar Team -> Stage (para ficarem sobre a supervisão do git)

Depois fazer o commit (este commit é contra o repositório local). 

No fim será necessário enviar as alterações para o repositório *origin*.

#### Passar as alterações locais para o respositório

Por fim, fazer o push das alterações para o Bitbucket. 

Sobre o projeto, fazer Team -> Remotes -> Push current to -> origin

#### Atualizar o branch local a partir do branch remoto

Antes de proceder a alterações, pode atualizar o branch local com as alterações que entretanto foram feitas no repositório origem.

Team --> Pull

### Correr o projeto localmente

Para correr o projeto localmente, é necessário ter um servidor HTTP (por exemplo, o Apache) configurado.

É necessário ter um proxy (eventualmente) configurado para ultrapassar as limitações de segurança do Javascript. Isto é, se o projeto corre localmente, mas alguns dos serviços WMS e WFS estão noutro domínio, é preciso usar um proxy.

Em /OpenLayers-2.12/examples/proxy.cgi já existe um proxy escrito em Python. Para se poder usar neste projeto é preciso fazer duas coisas:
* Alterar o proxy.cgi de modo a incluir 'webgis.di.uminho.pt' no array allowedHosts.
* Configurar o Apache de modo a que se possam correr cgi (executar programas) na pasta onde está o proxy.cgi.

#### Configurar o Apache

Editar `/etc/apache2/sites-enabled/000-default`

Acrescentar

        AddHandler cgi-script .cgi
        # Para poder executar o 
        # OpenLayers.ProxyHost = "/OpenLayers-2.12/examples/proxy.cgi?url=";
        <Directory "/var/www/OpenLayers-2.12/examples/">
                Options Indexes FollowSymLinks MultiViews +ExecCGI
                AllowOverride All
                Order allow,deny
                allow from all
        </Directory>

#### Testar o proxy

Para testar o proxy, basta abrir (http://localhost/OpenLayers-2.12/examples/proxy.cgi?url=http://openlayers.org). Se correr bem, aparece-lhe a página do OpenLayers sacada através do proxy.cgi.
