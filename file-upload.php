<?php
require_once 'DB.php';
// header('Content-type: application/json'); // o Firefox queixava-se...
// header("Content-Type: text/javascript; charset=utf-8");
header('Content-type: text/html'); // o Firefox precisa que seja text/html... cf. http://www.sencha.com/forum/showthread.php?256661-Submit-form-TypeError-problem

// sudo chown -R www-data:www-data uploads

// curl -X POST --form user_name=jgr --form fid=ocorrencias.1077 --form photo-path=@"images/legenda_exemplo.png" "http://softwarelivre.cm-agueda.pt/acessibilidades/file-upload.php"

/*
 * curl -X POST --form user_name=jgr --form fid=ocorrencias.1077 --form photo-path=@"/home/jgr/DropTrabalho/Dropbox/Camera Uploads/2013-07-28 18.55.07.jpg" "http://softwarelivre.cm-agueda.pt/acessibilidades/file-upload.php"
 * {"errors":{"is_uploaded_file":0},"caminho":"uploads\/10\/2013-07-28_18.55.07.jpg","success":true}
 * Se is_uploaded_file != 0, cf. http://php.net/manual/en/function.is-uploaded-file.php
 */

function make_thumb($src, $dest, $desired_width) {
	/* saber o tipo de imagem */
    $size=getimagesize($src);
    switch($size["mime"]){
        case "image/jpeg":
            $source_image = imagecreatefromjpeg($src); //jpeg file
        break;
        case "image/gif":
            $source_image = imagecreatefromgif($src); //gif file
      	break;
      	case "image/png":
          	$source_image = imagecreatefrompng($src); //png file
      	break;
    	default: 
        	return false;
    	break;
	}
	$width = imagesx($source_image);
	$height = imagesy($source_image);
	/* find the "desired height" of this thumbnail, relative to the desired width  */
	$desired_height = floor($height * ($desired_width / $width));
	/* create a new, "virtual" image */
	$virtual_image = imagecreatetruecolor($desired_width, $desired_height);
	/* copy source image at a resized size */
	imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
	/* create the physical thumbnail image to its destination */
	imagejpeg($virtual_image, $dest);
}

$servidor = $_SERVER['SERVER_NAME'];
$pos = strpos($servidor, 'localhost');
if ($pos === false) {
	$dsn = "pgsql://geobox:geobox2k13@amr.osgeopt.pt:5432/amr";
} else {
	$dsn = "pgsql://geobox:geobox@localhost:5432/ip";
}

$db = DB::connect($dsn, false);
if (DB::isError($db)) {
	$resposta["success"] = false;
	$resposta["errors"]["reason"] = $db -> getMessage();
	die(json_encode($resposta));
	// {"success":false,"errors":{"reason":"DB Error: connect failed"}}
}

if (isset($_POST)) {
	// $_POST["fid"] = ocorrencias.1077
	$fidstr = $_POST["fid"];
	$partes = explode(".", $fidstr);
	// $featuretype = $partes[0];
	$fid = $partes[1];
	$aux = (int)(intval($fid) / 100);
	// uploads/100 se $fid = 1087
	$pasta = "uploads/" . strval($aux);
	if (!is_dir($pasta)) {
		if (!mkdir($pasta)) {
			$resposta['success'] = 'false';
			$resposta["errors"]["reason"] = 'failed to create folder ' . $pasta;
			die(json_encode($resposta));
		} else {
			// s$srce criou aquela, tamém cria estas...
			mkdir($pasta . "/120");
			mkdir($pasta . "/400");
		}
	}

	if (isset($_FILES)) {
		$file_tmp = $_FILES['photo-path']['tmp_name'];
		$file_name = str_replace(' ', '_', $_FILES['photo-path']['name']);
		$file_size = $_FILES['photo-path']['size'];
		
		$erro = $_FILES['photo-path']['error'];
		// $resposta["debug"]["is_uploaded_file"] = $erro;
		
		$caminho = $pasta . "/$file_name";
		$resposta['caminho'] = $caminho;
		if (is_uploaded_file($file_tmp)) {
			if (move_uploaded_file($file_tmp, $caminho)) {
				$resposta['success'] = 'true';
			} else {
				$resposta['success'] = 'false';
				$resposta["errors"]["reason"] = 'move_uploaded_file failed';
			}
		} else {
			$resposta['success'] = 'false';
			$resposta["errors"]["reason"] = 'is_uploaded_file test failed';
		}
	}

	make_thumb($caminho, $pasta . "/120" . "/$file_name", 120);
	make_thumb($caminho, $pasta . "/400" . "/$file_name", 400);

	$sql = "INSERT into amr.fotografia (id_ocorrencia, pasta, caminho, user_name, tamanho) values (" . $fid . ", '" . $pasta . "', '" . $file_name . "', '" . $_POST["user_name"] . "', " . $file_size . " )";
	$res = $db -> query($sql);
	if (DB::isError($res)) {
		$resposta["success"] = false;
		$resposta["errors"]["reason"] = $res -> getMessage();
		$resposta["errors"]["sql"] = $sql;
		die(json_encode($resposta));
		// {"success":false,"errors":{"reason":"DB Error: syntax error"}}
	} else {
		// SUCESSO!
		$resposta["success"] = true;
	}

} else {
	$resposta['success'] = 'false';
	$resposta["errors"]["reason"] = '_POST is not defined';
}
echo json_encode($resposta);
?>