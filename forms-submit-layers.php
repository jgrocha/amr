<?php
require_once 'DB.php';
// o formato retornado vai ser sempre JSON
header('Content-type: application/json');

/* ligar à base de dados postgis */
$servidor = $_SERVER['SERVER_NAME'];
$pos = strpos($servidor, 'localhost');
if ($pos === false) {
	$dsn = "pgsql://geobox:geobox2k13@amr.osgeopt.pt:5432/amr";
} else {
	$dsn = "pgsql://geobox:geobox@localhost:5432/ip";
}

$tabela = 'tema';

$db = DB::connect($dsn, false);
if (DB::isError($db)) {
	$result["success"] = false;
	$result["errors"]["reason"] = $db -> getMessage();
	die(json_encode($result));
	// {"success":false,"errors":{"reason":"DB Error: connect failed"}}
}

$action = (empty($_GET['action'])) ? 'read' : $_GET['action'];

if ($action == 'create') {
	/* Ora vem tudo dentro de [{"nome":"planet_osm_point","titulo":"planet_osm_point","layer":"topp:planet_osm_point","url":"http://localhost:80/geoserver/ows?SERVICE=WMS&","singletile":false,"observacoes":"Acrescentado via web","grupo":"Outros","tipo":"images/Mf_area.png","srid":"3763","dataalteracao":"2012-03-07T00:00:00","activo":true,"visivel":true,"ord":"","estilo":"","qtip":"","regulamento":""},{"nome":"João","titulo":"Cont_AAD_CAOP2011","layer":"sf:caop","url":"http://localhost:80/geoserver/ows?SERVICE=WMS&","singletile":false,"observacoes":"Acrescentado via web","grupo":"Outros","tipo":"images/Mf_area.png","srid":"3763","dataalteracao":"2012-03-07T00:00:00","activo":true,"visivel":true,"ord":"","estilo":"","qtip":"","regulamento":""}]
	 $aux = json_decode($_POST['camadas'], true);
	 $obj = $aux[0];
	 *
	 * Ou vem 'normalmente' como {"nome":"planet_osm_point","titulo":"planet_osm_point","layer":"topp:planet_osm_point","url":"http://localhost:80/geoserver/ows?SERVICE=WMS&","singletile":false,"observacoes":"Acrescentado via web","grupo":"Outros","tipo":"images/Mf_area.png","srid":"3763","dataalteracao":"2012-03-07T00:00:00","activo":true,"visivel":true,"ord":"","estilo":"","qtip":"","regulamento":""},{"nome":"João","titulo":"Cont_AAD_CAOP2011","layer":"sf:caop","url":"http://localhost:80/geoserver/ows?SERVICE=WMS&","singletile":false,"observacoes":"Acrescentado via web","grupo":"Outros","tipo":"images/Mf_area.png","srid":"3763","dataalteracao":"2012-03-07T00:00:00","activo":true,"visivel":true,"ord":"","estilo":"","qtip":"","regulamento":""}
	 *
	 $obj = json_decode($_POST['camadas'], true);
	 */
	$aux = json_decode($_POST['camadas'], true);
	// $ncampos = sizeof($aux);
	$ncampos = count($aux, COUNT_RECURSIVE);
	if ($ncampos > 5) {
		$obj = $aux;
	} else {
		$obj = $aux[0];
	}
	$result["partes"] = $ncampos;

	$nfields = 0;
	$chaves = "";
	$valores = "";
	foreach ($obj as $key => $value) {
		$sep = ($nfields == 0) ? '' : ', ';
		if ($key != '' && $value != '') {
			$chaves .= $sep . $key;
			$valores .= $sep . "'" . $value . "'";
			$nfields += 1;
		}
	}
	$sql = "INSERT into " . $tabela . " (" . $chaves . ") values (" . $valores . " ) ";

	$res = $db -> query($sql);
	if (DB::isError($res)) {
		$result["success"] = false;
		$result["errors"]["reason"] = $res -> getMessage();
		$result["errors"]["query"] = $sql;
		die(json_encode($result));
		// {"success":false,"errors":{"reason":"DB Error: syntax error"}}
	} else {
		// sacar o ID inserido
		$sql2 = "select lastval()";
		$res2 = $db -> query($sql2);
		$returning = $res2 -> fetchRow(DB_FETCHMODE_ASSOC);
		// echo "Registo com o gid " . $returning['lastval'] . " inserido.";
		$query = "select * from " . $tabela;
		$query .= " where id = " . $returning['lastval'];
		$resQuery = $db -> query($query);
		if (DB::isError($resQuery)) {
			$result["success"] = false;
			$result["errors"]["reason"] = $resQuery -> getMessage();
			$result["errors"]["query"] = $query;
			die(json_encode($result));
		}
		$row = $resQuery -> fetchRow(DB_FETCHMODE_ASSOC);
		// SUCESSO!
		// passo todo o resultado para o cliente

		/*
		 foreach ($row as $key => $value) {
		 $result["data"][$key] = $value;
		 }
		 $result["success"] = true;
		 echo json_encode($result);
		 */

		$tabela = array();
		array_push($tabela, $row);
		$result["camadas"] = $tabela;
		$result["success"] = true;
		echo json_encode($result);
	}
}

if ($action == 'update') {
	$obj = json_decode($_POST['camadas'], true);
	// retorna um array
	$id = $obj['id'];
	unset($obj['id']);
	unset($obj['dataalteracao']);

	/*
	 * tratamento dos booleanos:
	 * o JSON que chega é: 	{"activo":false,"id":"102"}
	 */

	$nfields = 0;
	$update = "";
	foreach ($obj as $key => $value) {
		$sep = ($nfields == 0) ? '' : ', ';
		switch (gettype($value)) {
			case "boolean" :
				$valor = $value ? "'1'" : "'0'";
				break;
			case "integer" :
			case "double" :
				$valor = $value;
				break;
			case "string" :
				$valor = "'" . $value . "'";
				break;
			case "NULL" :
				$valor = "NULL";
				break;
			default :
				$valor = "'" . $value . "'";
		}
		$update .= $sep . $key . "=" . $valor;
		$nfields += 1;
	}

	$sql = "update " . $tabela . " set " . $update . " where id = " . $id;

	$res = $db -> query($sql);
	if (DB::isError($res)) {
		$result["success"] = false;
		$result["errors"]["reason"] = $res -> getMessage();
		$result["errors"]["query"] = $sql;
		die(json_encode($result));
	} else {
		$linhasAfetadas = $db -> affectedRows();
		$result["total"] = $linhasAfetadas;
		if ($linhasAfetadas > 0) {
			$result["feedback"] = "A camada '" . $id . "' foi alterada com sucesso.";
		} else {
			$result["feedback"] = "A camada '" . $id . "' não foi alterada.";
		}
		$result["success"] = true;
		$result["sql"] = $sql;
		echo json_encode($result);
	}

}

if ($action == 'destroy') {
	// fica "103"
	// $id = $_POST['camadas'];

	$id = json_decode($_POST['camadas'], true);

	$sql = "delete from " . $tabela . " where id = " . $id;

	$res = $db -> query($sql);
	if (DB::isError($res)) {
		$result["success"] = false;
		$result["errors"]["reason"] = $res -> getMessage();
		$result["errors"]["query"] = $sql;
		die(json_encode($result));
		// {"success":false,"errors":{"reason":"DB Error: syntax error"}}
	} else {
		$linhasRemovidas = $db -> affectedRows();
		$result["total"] = $linhasRemovidas;
		if ($linhasRemovidas > 0) {
			$result["feedback"] = "A camada '" . $id . "' foi removida com sucesso.";
		} else {
			$result["feedback"] = "A camada '" . $id . "' não foi removida.";
		}
		$result["success"] = true;
		$result["sql"] = $sql;
		echo json_encode($result);
	}
}

if ($action == 'read') {
	/*
	 * Pode chegar um parâmetro 'query', com um termo de pesquisa, do Ext.ux.form.SearchField
	 */
	if (isset($_POST['query']) && strlen(trim($_POST['query'])) > 0) {
		$pesquisa = $_POST['query'];
	} else {
		$pesquisa = "";
	}
	if ($pesquisa != "") {
		$where .= sprintf("where titulo ilike '%%%s%%' or ", $pesquisa);
		$where .= sprintf("layer ilike '%%%s%%' or ", $pesquisa);
		$where .= sprintf("grupo ilike '%%%s%%' or ", $pesquisa);
		$where .= sprintf("regulamento ilike '%%%s%%' or ", $pesquisa);
		$where .= sprintf("observacoes ilike '%%%s%%' ", $pesquisa);
	} else {
		$where = "";
	}
	$query = "select id, ord, titulo, layer, grupo, url, tipo, srid, estilo, qtip, ";
	$query .= "CASE WHEN singletile THEN 1 ELSE 0 END as singletile, CASE WHEN activo THEN '1' ELSE '0' END as activo, ";
	$query .= "regulamento, observacoes, CASE WHEN visivel THEN '1' ELSE '0' END as visivel, CASE WHEN isbaselayer THEN '1' ELSE '0' END as isbaselayer, dataalteracao ";
	$query .= "from " . $tabela . " " . $where;

	// $query .= "where activo = TRUE ";
	$query .= "order by id asc ";
	$res = $db -> query($query);
	if (DB::isError($res)) {
		die($res -> getMessage());
	}
	$tabela = array();
	while ($row = $res -> fetchRow(DB_FETCHMODE_ASSOC)) {
		array_push($tabela, $row);
	}
	$result["camadas"] = $tabela;
	echo json_encode($result);
	$db -> disconnect();

}
?>
