/**
 * Copyright (c) 2008-2010 The Open Source Geospatial Foundation
 *
 * Published under the BSD license.
 * See http://svn.geoext.org/core/trunk/geoext/license.txt for the full text
 * of the license.
 */

/** api: example[wms-capabilities]
 *  WMS Capabilities Store
 *  ----------------------
 *  Create layer records from WMS capabilities documents.
 */

var store;
var gridPDM;

Ext.onReady(function() {

	OpenLayers.DOTS_PER_INCH = 25.4 / 0.28;
	// OpenLayers.ProxyHost = "/cgi-bin/proxy.original.cgi?url=";
	OpenLayers.ProxyHost = "/cgi-bin/proxy.unquote.cgi?url=";
	// OpenLayers.ProxyHost = "";

	// create a new WMS capabilities store

	var pombinho = new Ext.form.ComboBox({
		width : 300,
		xtype : 'combo',
		mode : 'local',
		fieldLabel : 'URL para pedir o GetCapabilities',
		store : new Ext.data.SimpleStore({
			fields : ['src', 'geometria'],
			data : [['1', 'http://localhost/geoserver/wms?request=getCapabilities&version=1.3.0&service=WMS'], ['2', 'http://84.91.3.11/geoserver/wms?request=getCapabilities&version=1.3.0&service=WMS'], ['3', 'http://geoportal.cm-agueda.pt/geoserver/wms?request=getCapabilities&version=1.3.0&service=WMS'], ['4', 'http://mapas.igeo.pt/ows/caop/continente?request=GetCapabilities&SERVICE=WMS&VERSION=1.1.1']]
		}),
		valueField : 'geometria',
		displayField : 'geometria',
		triggerAction : 'all',
		editable : true
	});

	// pombinho.setValue(pombinho.getStore().getAt(1));
	pombinho.setValue('http://localhost/geoserver/wms?request=getCapabilities&version=1.3.0&service=WMS');
	// pombinho.selectByValue('1', true);

	var form_servidor_wms = new Ext.FormPanel({
		region : 'west',
		// renderTo : "pedidowms",
		labelAlign : 'top',
		title : 'Pedido de GetCapabilities',
		bodyStyle : 'padding:5px',
		width : 320,
		height : 300,
		items : [pombinho],
		buttons : [{
			text : 'Pedir o GetCapabilities',
			handler : function() {
				if (form_servidor_wms.form.items.items[0].value.length < 10) {
					Ext.MessageBox.alert('URL em falta', 'Tem que preencher o URL do servidor de onde pretende utilizar camadas.');
				} else {
					// var novourl = "http://localhost/cgi-bin/proxy.cgi?url=" + escape(form_servidor_wms.form.items.items[0].value);
					var novourl = OpenLayers.ProxyHost + escape(form_servidor_wms.form.items.items[0].value);
					console.log(novourl);
					storeCapabilities.proxy.conn.url = novourl;
					storeCapabilities.load();
				}
			}
		}]
	});

	var storeCapabilities = new GeoExt.data.WMSCapabilitiesStore({
		// url : "http://mapas.igeo.pt/ows/vgs?SERVICE=WMS&REQUEST=GetCapabilities&VERSION=1.1.1",
		url : OpenLayers.ProxyHost + escape("http://mapas.igeo.pt/ows/caop/continente?request=GetCapabilities&SERVICE=WMS&VERSION=1.1.1"),
		autoLoad : false
	});

	// load the store with records derived from the doc at the above url
	// store.load();

	/*
	var pesquisaLayers = new Ext.ux.form.SearchField({
	store : storeCapabilities,
	width : 120
	});
	*/

	// create a gridWMS to display records from the store
	var gridWMS = new Ext.grid.GridPanel({
		collapsible : false,
		region : 'center',
		height : 300,
		width : 600,
		title : "WMS Capabilities",
		store : storeCapabilities,
		selModel : new Ext.grid.RowSelectionModel({
			singleSelect : true
		}),
		columns : [{
			header : "Title",
			dataIndex : "title",
			sortable : true
		}, {
			header : "Name",
			dataIndex : "name",
			sortable : true
		}, {
			header : "Queryable",
			dataIndex : "queryable",
			sortable : true,
			width : 70
		}, {
			id : "description",
			header : "Description",
			dataIndex : "abstract"
		}],
		autoExpandColumn : "description",
		// renderTo : "capgrid",

		listeners : {
			rowdblclick : mapPreview
		},
		// tbar : ['Pesquisar: ', ' ', pesquisaLayers],
		bbar : new Ext.PagingToolbar({
			pageSize : 25,
			store : storeCapabilities,
			displayInfo : true,
			displayMsg : 'Resultados {0} a {1} de {2}',
			emptyMsg : "Sem resultados para apresentar"
		})
	});

	function mapPreview(grid, index) {
		var record = grid.getStore().getAt(index);
		var layer = record.getLayer().clone();
		console.log(layer);

		var win = new Ext.Window({
			title : "Antevisão da camada " + record.get("title"),
			width : 512,
			height : 512,
			layout : "fit",
			items : [{
				xtype : "gx_mappanel",
				layers : [layer],
				extent : record.get("llbbox")
			}]
		});
		win.show();
	}

	var writer = new Ext.data.JsonWriter({
		writeAllFields : false, // só passa os campos alterados (e o id!)
		encode : true
	});

	// http://www.loiane.com/2010/03/extjs-e-spring-framework-exemplo-de-um-crud-grid/
	// http://dev.sencha.com/deploy/ext-1.1.1/docs/output/Ext.data.Record.html

	var GeoLayer = Ext.data.Record.create([{
		name : 'id',
		type : 'int'
	}, {
		name : 'ord',
		type : 'int'
	}, /*{
	 name : 'nome',
	 type : 'string'
	 },*/
	{
		name : 'titulo',
		type : 'string'
	}, {
		name : 'layer',
		type : 'string'
	}, {
		name : 'grupo',
		type : 'string'
	}, {
		name : 'url',
		type : 'string'
	}, {
		name : 'tipo',
		type : 'string'
	}, {
		name : 'srid',
		type : 'int'
	}, {
		name : 'estilo',
		type : 'string'
	}, {
		name : 'qtip',
		type : 'string'
	}, {
		name : 'singletile',
		type : 'bool'
	}, {
		name : 'activo',
		type : 'bool'
	}, {
		name : 'regulamento',
		type : 'string'
	}, {
		name : 'observacoes',
		type : 'string'
	}, {
		name : 'visivel',
		type : 'bool'
	}, {
		name : 'isbaselayer',
		type : 'bool'
	}, {
		name : 'dataalteracao',
		type : 'date',
		dateFormat : 'c'	// impecável: a data vem no formato ISO8601
	}]);

	var storeLayersPDM = new Ext.data.JsonStore({
		autoSync : true,
		autoLoad : true,
		url : 'forms-submit-layers.php',
		root : 'camadas',
		idProperty : 'id',
		proxy : new Ext.data.HttpProxy({
			api : {
				create : 'forms-submit-layers.php?action=create', // Called when saving new records
				read : 'forms-submit-layers.php?action=read', // Called when reading existing records
				update : 'forms-submit-layers.php?action=update', // Called when updating existing records
				destroy : 'forms-submit-layers.php?action=destroy' // Called when deleting existing records
			},
			listeners : {
				exception : function(dataProxy, type, action, options, response, arg) {
					console.log('PROXY ERROR type=' + type);
					console.log('status=' + response.status + '  statusText=' + response.statusText);
					console.log(' arg=');
					console.log(arg);
				}
			}
		}),
		writer : writer,
		fields : GeoLayer
	});
	// storeLayersPDM.load();

	var pesquisaLayersPDM = new Ext.ux.form.SearchField({
		store : storeLayersPDM,
		width : 120
	});

	var editor = new Ext.ux.grid.RowEditor({
		saveText : 'Atualiza'
	});

	// create a grid to display records from the store
	// http://www.sencha.com/learn/the-grid-component/
	// http://www.delphifaq.com/faq/javascript/extjs/f3555.shtml

	// var gridPDM = new Ext.grid.GridPanel({
	gridPDM = new Ext.grid.EditorGridPanel({
		region : 'center',
		autosave : true,
		height : 300,
		width : 300,
		title : "Gestão das Camadas do visualizador",
		store : storeLayersPDM,
		plugins : [editor],
		selModel : new Ext.grid.RowSelectionModel({
			singleSelect : true
		}),
		tbar : ['Pesquisar: ', ' ', pesquisaLayersPDM, {
			iconCls : 'icon-user-add',
			text : 'Adiciona camada',
			handler : function() {
				// console.log('Iria acrescentar um layer...');
				var linhas = gridWMS.getSelectionModel().getCount();
				if (linhas > 0) {
					var s = gridWMS.getSelectionModel().getSelections();
					r = s[0];
					// só vai buscar o primeiro selecionado, se houver mais do que um...
					console.log(r.data.layer);
					var e = new GeoLayer({
						nome : Ext.util.Format.trim(r.data.layer.name),
						titulo : Ext.util.Format.trim(r.data.layer.name),
						layer : r.data.layer.params.LAYERS,
						url : r.data.layer.url,
						singletile : r.data.layer.singleTile,
						observacoes : 'Acrescentado via web',
						grupo : 'Outros',
						tipo : 'images/Mf_area.png',
						srid : '3763',
						dataalteracao : new Date().format('Y-m-d'),
						activo : true,
						visivel : true
					});
					editor.stopEditing();
					storeLayersPDM.insert(0, e);
					gridPDM.getView().refresh();
					gridPDM.getSelectionModel().selectRow(0);
					// não está a dar editar logo...
					// editor.startEditing(0);
				} else {
					Ext.MessageBox.alert('Camada', 'Tem que selecionar uma camada dentro dos resultados do GetCapabilities');
				}
			}
		}, {
			ref : '../removeBtn', // para se poder usar: gridPDM.removeBtn.(setDisabled)...)
			iconCls : 'icon-user-delete',
			text : 'Remove camada',
			disabled : true,
			handler : function() {
				editor.stopEditing();
				// voltou a ter um rowselectionmodel
				var r = gridPDM.getSelectionModel().getSelections();
				gridPDM.getStore().remove(r);
			}
		} /*, {
		 iconCls : 'icon-user-save',
		 text : 'Guarda alterações',
		 disabled : true,
		 handler : function() {
		 var data = [];
		 Ext.each(gridPDM.getStore().getModifiedRecords(), function(record) {
		 data.push(record.data);
		 });
		 console.log('Vou actualizar os seguintes registos:');
		 console.log(data);
		 storeLayersPDM.save();
		 // storeLayersPDM.commitChanges();
		 }
		 } */],
		columns : [{
			header : "ID",
			dataIndex : "id",
			width : 40,
			sortable : true,
			editor : {
				xtype : 'numberfield'
			}
		}, {
			header : "Ord",
			dataIndex : "ord",
			width : 40,
			sortable : true,
			editor : {
				xtype : 'numberfield'
			}
		}, /*{
		 header : "Nome",
		 dataIndex : "nome",
		 sortable : true,
		 editor : {
		 xtype : 'textfield',
		 allowBlank : false
		 }
		 },*/
		{
			header : "Titulo",
			dataIndex : "titulo",
			sortable : true,
			width : 160,
			editor : {
				xtype : 'textfield'
			}
		}, {
			header : "Layer no servidor",
			dataIndex : 'layer',
			type : 'string',
			width : 320,
			editor : {
				xtype : 'textfield',
				allowBlank : false
			}
		}, {
			header : "Grupo a que pertence",
			dataIndex : 'grupo',
			type : 'string',
			width : 360,
			editor : {
				xtype : 'textfield',
				allowBlank : false
			}
		}, {
			header : "URL",
			dataIndex : "url",
			width : 240,
			sortable : true,
			editor : {
				xtype : 'textfield',
				allowBlank : false,
				xtype : 'textfield'
				// vtype : 'url'
			}
		}, {
			header : "Geometria[s]",
			dataIndex : 'tipo',
			width : 160,
			type : 'string',
			editor : {
				xtype : 'combo',
				store : new Ext.data.ArrayStore({
					fields : ['src', 'geometria'],
					data : [['images/Mf_area.png', 'Mf_area'], ['images/Mf_node_area.png', 'Mf_node_area'], ['images/Mf_node_way_area_PB.png', 'Mf_node_way_area_PB'], ['images/Mf_node_way_area.png', 'Mf_node_way_area'], ['images/Mf_node_way.png', 'Mf_node_way'], ['images/Mf_node.png', 'Mf_node'], ['images/Mf_raster.png', 'Mf_raster'], ['images/Mf_way_area.png', 'Mf_way_area'], ['images/Mf_way.png', 'Mf_way']]
				}),
				displayField : 'geometria',
				valueField : 'src',
				mode : 'local',
				typeAhead : false,
				triggerAction : 'all',
				lazyRender : true,
				emptyText : 'Escolha a geometria mais adequada'
			}

		}, {
			header : "tile único?",
			dataIndex : 'singletile',
			width : 80,
			xtype : 'booleancolumn',
			trueText : 'Sim',
			falseText : 'Não',
			editor : {
				xtype : 'checkbox'
			}
		}, {
			header : "Activo?",
			dataIndex : 'activo',
			width : 80,
			xtype : 'booleancolumn',
			trueText : 'Sim',
			falseText : 'Não',
			editor : {
				xtype : 'checkbox'
			}
		}, {
			header : "Data alteração",
			dataIndex : 'dataalteracao',
			// 			type : 'date',
			editor : new Ext.form.DateField({
				name : 'dataalteracao',
				width : 90,
				allowBlank : false,
				format : 'Y-m-dTH:i:s.u',
				renderer : Ext.util.Format.dateRenderer('Y-m-dTH:i:s.u')
			})
		}, {
			header : "Visível?",
			dataIndex : 'visivel',
			width : 80,
			// type : 'boolean',
			xtype : 'booleancolumn',
			trueText : 'Sim',
			falseText : 'Não',
			editor : {
				xtype : 'checkbox'
			}
		}, {
			header : "Camada Base?",
			dataIndex : 'isbaselayer',
			width : 80,
			// type : 'boolean',
			xtype : 'booleancolumn',
			trueText : 'Sim',
			falseText : 'Não',
			editor : {
				xtype : 'checkbox'
			}
		}, {
			header : "srid",
			dataIndex : 'srid',
			width : 40,
			type : 'integer',
			editor : {
				xtype : 'numberfield'
			}
		}, {
			header : "Estilo SLD",
			dataIndex : 'estilo',
			type : 'string',
			editor : {
				xtype : 'textfield'
			}
		}, {
			header : "Tooltip",
			dataIndex : 'qtip',
			type : 'string',
			editor : {
				xtype : 'textfield'
			}
		}, {
			header : "Regulamento",
			dataIndex : 'regulamento',
			type : 'string',
			editor : {
				xtype : 'textfield'
			}
		}, {
			header : "Observações",
			dataIndex : 'observacoes',
			type : 'string',
			editor : {
				xtype : 'textfield'
			}
		}],
		// autoExpandColumn : "description",
		// renderTo : "capgridpdm",

		listeners : {
			rowdblclick : mapPreviewPDM
		},
		bbar : new Ext.PagingToolbar({
			pageSize : 25,
			store : storeLayersPDM,
			displayInfo : true,
			displayMsg : 'Resultados {0} a {1} de {2}',
			emptyMsg : "Sem resultados para apresentar"
		})
	});

	gridPDM.getSelectionModel().on('selectionchange', function(sm) {
		gridPDM.removeBtn.setDisabled(sm.getCount() < 1);
		// var changedRecords = gridPDM.getStore().getModifiedRecords();
		// gridPDM.removeBtn.setDisabled(changedRecords < 1);
		// console.log(changedRecords);
		// console.log(sm.selection);
	});
	function mapPreviewPDM(grid, index) {
		// cancela a edição
		editor.stopEditing();
		//
		var r = grid.getStore().getAt(index);
		console.log(r);
		// var layer = record.getLayer().clone();
		var layer = new OpenLayers.Layer.WMS(r.data['titulo'], r.data['url'],
		// parameters for the WMS service itself
		{
			layers : r.data['layer'],
			format : 'image/png',
			transparent : true,
			resolutions : [140.0, 70.0, 27.999999999999996, 13.999999999999998, 6.999999999999999, 2.8, 1.4, 0.27999999999999997, 0.13999999999999999]
		},
		// optional second dictionary object, contains OpenLayers Layer settings
		{
			singleTile : r.data['singletile'],
			ratio : 1,
			isBaseLayer : true, // false,
			visibility : true, // r.data['visivel'],
			group : r.data['grupo'],
			iconCls : "xnd-icon",
			icon : r.data['tipo'],
			qtip : r.data['qtip'],
			tileSize : new OpenLayers.Size(256, 256),
			projection : new OpenLayers.Projection('EPSG:3763'),
			units : 'meters'
		});

		console.log(layer);
		var aux = layer.clone();

		var boundsPortugal = new OpenLayers.Bounds(-119191.40749999962, -300404.80399999936, 162129.08110000013, 276083.7674000006);
		var optionsMapinha = {
			maxExtent : boundsPortugal,
			// scales : [250000, 100000, 50000, 25000, 10000, 5000, 1000, 500],
			resolutions : [140.0, 70.0, 27.999999999999996, 13.999999999999998, 6.999999999999999, 2.8, 1.4, 0.27999999999999997, 0.13999999999999999],
			// maxResolution : "auto",
			projection : "EPSG:3763", // string
			displayProjection : new OpenLayers.Projection("EPSG:3763"),
			title : 'Mapa',
			units : 'm',
			allOverlays : true
		};
		var mapinha = new OpenLayers.Map(optionsMapinha);

		var win = new Ext.Window({
			title : "Antevisão da camada " + r.data['titulo'],
			width : 512,
			height : 512,
			layout : "fit",
			items : [{
				xtype : "gx_mappanel",
				layers : [aux],
				map : mapinha,
				extent : new OpenLayers.Bounds(-21581.79, 88376, -7840, 116536)
			}]
			/*
			 items : [{
			 html : "Viva o Jorge " + grid.title + " linha " + index,
			 xtype : "panel"
			 }]
			 */
		});
		win.show();
	}

	var viewport = new Ext.Viewport({
		layout : "border",
		defaults : {
			collapsible : true,
			split : true
		},
		// height : '800',
		// width : '800',
		items : [{
			title : "Adicionar camadas",
			region : 'north',
			height : 280,
			layout : "border",
			items : [form_servidor_wms, gridWMS]
		}, gridPDM]
	});
	viewport.doLayout();
});
