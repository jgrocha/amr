<?php
require_once 'DB.php';

$servidor = $_SERVER['SERVER_NAME'];
$pos = strpos($servidor, 'localhost');
if ($pos === false) {
	$dsn = "pgsql://geobox:geobox2k13@amr.osgeopt.pt:5432/amr";
} else {
	$dsn = "pgsql://geobox:geobox@localhost:5432/ip";
}

$db = DB::connect($dsn, false);
if (DB::isError($db)) {
	$resposta = $db -> getMessage();
	die($resposta);
}

if (isset($_POST)) {
	// $_POST["fid"] = ocorrencias.1077
	$fidstr = $_POST["fid"];
	$partes = explode(".", $fidstr);
	// $featuretype = $partes[0];
	$id_ocorrencia = $partes[1];

	// $query = "select o.*, e.estado from amr.ocorrencias o, amr.estado_ocorrencia e where id_ocorrencia = $id_ocorrencia and o.id_estado=e.id_estado";
	$query = "select o.*, e.estado, now()-data_hora as haquantotempo from amr.ocorrencias o, amr.estado_ocorrencia e where id_ocorrencia = $id_ocorrencia and o.id_estado=e.id_estado";
	$res = $db -> query($query);
	if (DB::isError($res)) {
		die($res -> getMessage());
	}
	$row = $res -> fetchRow(DB_FETCHMODE_ASSOC);
	$id_estado = $row['id_estado'];
	$estado = $row['estado'];
	$data_hora = $row['data_hora'];
	$haquantotempo = $row['haquantotempo'];
	$titulo = $row['titulo'];
	$participacao = $row['participacao'];
	$user_name = $row['user_name'];

	echo '<div class="ocorrencias">';

	echo '<div class="ocorrencia">';
	echo '<div class="avatar-impar">';
	$avatar = "http://graph.facebook.com/" . $row['userid'] . "/picture";
	echo '<img src="' . $avatar . '"/>';
	echo '</div>';
	echo '<div class="participacao-impar">' . $participacao;

	$query = "select * from amr.fotografia where id_ocorrencia = " . $id_ocorrencia;
	$res = $db -> query($query);
	if (DB::isError($res)) {
		die($res -> getMessage());
	}
	while ($row = $res -> fetchRow(DB_FETCHMODE_ASSOC)) {
		$thumb = $row['pasta'] . '/120/' . $row['caminho'];
		$normal = $row['pasta'] . '/400/' . $row['caminho'];
		echo '<a href="' . $normal . '" target="_blank">';
		echo '<img src="' . $thumb . '"></img>';
		echo '</a>';
	}

	echo '<div class="quem">Reportado por: ' . $user_name . '</div>';
	echo '<div class="quando">Reportado em: ' . $data_hora . ' (há ' . $haquantotempo . ')</div>';

	echo '</div>'; // participacao-impar
	echo '</div>'; // ocorrencia
		

	echo '<div style="clear: both;"></div>';

	$query = "select *, now()-data_hora as haquantotempo from amr.comentarios where id_ocorrencia = " . $id_ocorrencia;
	$query .= " order by data_hora";
	$res = $db -> query($query);
	if (DB::isError($res)) {
		die($res -> getMessage());
	}

	$cont = 2;
	while ($row = $res -> fetchRow(DB_FETCHMODE_ASSOC)) {
		if (($cont % 2) == 1) {
			$style = '-impar';
		} else {
			$style = '-par';
		}

		echo '<div class="ocorrencia">';

		echo '<div class="avatar' . $style . '">';
		$avatar = "http://graph.facebook.com/" . $row['userid'] . "/picture";
		echo '<img src="' . $avatar . '"/>';
		echo '</div>';

		echo '<div class="participacao' . $style . '">';
		echo '<div class="comentario">' . $row['comentario'];
		echo '<div class="quem">Reportado por <b>' . $row['user_name'] . '</b>';
		echo ' em ' . $row['data_hora'] . ' (há ' . $row['haquantotempo'] . ')</div>';
		echo '</div>'; // comentario
		echo '</div>'; // participação
		echo '</div>'; // ocorrencia
		echo '<div style="clear: both;"></div>';
		$cont = $cont+1;
	}
	echo '</div>'; // ocorrencias
} else {
	die('Parâmetros insuficientes');
}
?>