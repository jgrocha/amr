<?php
/*
 * Para testar:
 * curl -d query=pedro http://localhost/git/amr/getOcorrenciasJson.php
 */
require_once 'DB.php';
$servidor = $_SERVER['SERVER_NAME'];
$pos = strpos($servidor, 'localhost');
if ($pos === false) {
	$dsn = "pgsql://geobox:geobox2k13@amr.osgeopt.pt:5432/amr";
} else {
	$dsn = "pgsql://geobox:geobox@localhost:5432/ip";
}

$db = DB::connect($dsn, false);
if (DB::isError($db)) {
	$resposta["success"] = false;
	$resposta["errors"]["reason"] = $db -> getMessage();
	die(json_encode($resposta));
}

if (isset($_GET['start']) && strlen(trim($_GET['start'])) > 0) {
	$start = $_GET['start'];
} else {
	$start = 0;
}
if (isset($_GET['limit']) && strlen(trim($_GET['limit'])) > 0) {
	$limit = $_GET['limit'];
} else {
	$limit = 25;
}
if ($limit > 50) {
	$limit = 50;
}
if (isset($_GET['sort']) && strlen(trim($_GET['sort'])) > 0) {
	if (isset($_GET['dir']) && strlen(trim($_GET['dir'])) > 0) {
		$ordenar = " order by " . $_GET['sort'] . " " . $_GET['dir'];
	} else {
		$ordenar = " order by " . $_GET['sort'];
	}
} else {
	$ordenar = " order by id_ocorrencia DESC ";
}
if (isset($_GET['query']) && strlen(trim($_GET['query'])) > 0) {
	$pesquisa = $_GET['query'];
} else {
	$pesquisa = "";
}
if ($pesquisa != "") {
	$extra = sprintf("titulo ilike '%%%s%%' or participacao ilike '%%%s%%' or user_name ilike '%%%s%%'", $pesquisa, $pesquisa, $pesquisa);
	$where = " where " . $extra;
} else {
	$where = " ";
}

// The PHP script just use query=<whatever>
// http://localhost/git/amr/getOcorrenciasJson.php?_dc=1352549151645&page=1&start=0&limit=50&callback=Ext.data.JsonP.callback1
$queryConta = "select count(*) from amr.ocorrencias";
$queryConta .= $where;

// echo "$queryConta" . "\n";
$res = $db -> query($queryConta);
$row = $res -> fetchRow(DB_FETCHMODE_ASSOC);
$total = $row['count'];
// echo "$total" . "\n";

if ($pesquisa != "") {
	$extra = sprintf("titulo ilike '%%%s%%' or participacao ilike '%%%s%%' or user_name ilike '%%%s%%'", $pesquisa, $pesquisa, $pesquisa);
	$where = " where o.id_estado=e.id_estado and (" . $extra . ")";
} else {
	$where = " where o.id_estado=e.id_estado";
}

// select substring(xmlelement(name x, data_hora)::varchar from 4 for length(xmlelement(name x, data_hora)::varchar)-7)  from amr.comentarios

$query = "select o.id_ocorrencia, substring(xmlelement(name x, o.data_hora)::varchar from 4 for length(xmlelement(name x, data_hora)::varchar)-7) as data_hora , o.id_estado, o.id_tipo_ocorrencia, o.titulo, o.participacao, st_astext(o.the_geom) as the_geom, o.user_name, e.estado, now()-data_hora as haquantotempo from amr.ocorrencias o, amr.estado_ocorrencia e ";
$query .= $where;
$query .= $ordenar;
$query .= " LIMIT " . $limit . " OFFSET " . $start;

// echo "$query" . "\n";
$res = $db -> query($query);
if (DB::isError($res)) {
	$resposta["success"] = false;
	$resposta["errors"]["reason"] = $res -> getMessage();
	die(json_encode($resposta));
}
$tabela = array();
while ($row = $res -> fetchRow(DB_FETCHMODE_ASSOC)) {
	array_push($tabela, $row);
}
$resposta["ocorrencias"] = $tabela;
$resposta["success"] = true;
$resposta["total"] = $total;
echo json_encode($resposta);
?>