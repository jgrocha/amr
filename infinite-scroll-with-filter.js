Ext.Loader.setConfig({
	enabled : true
});

Ext.Loader.setPath('Ext.ux', '/extjs-4.1.1/examples/ux/');
// /extjs-4.1.1/examples/ux/form/SearchField.js
Ext.require(['Ext.grid.*', 'Ext.data.*', 'Ext.util.*', 'Ext.grid.PagingScroller', 'Ext.ux.form.SearchField']);

Ext.onReady(function() {
	Ext.define('Ocorrencia', {
		extend : 'Ext.data.Model',
		fields : [{
			name : 'titulo',
		}, {
			name : 'participacao',
		}, {
			name : 'id_tipo_ocorrencia',
			type : 'int'
		}, {
			name : 'user_name',
		}, {
			name : 'id_estado',
			type : 'int'
		}, {
			name : 'data_hora',
			type : 'date'
			// dateFormat : 'timestamp'
		}],
		idProperty : 'id_ocorrencia'
	});

	// create the Data Store
	var store = Ext.create('Ext.data.Store', {
		id : 'store',
		model : 'Ocorrencia',
		// allow the grid to interact with the paging scroller by buffering
		buffered : true,

		// The topics-remote.php script appears to be hardcoded to use 50, and ignores this parameter, so we
		// are forced to use 50 here instead of a possibly more efficient value.
		pageSize : 50,

		// This web service seems slow, so keep lots of data in the pipeline ahead!
		leadingBufferZone : 1000,
		proxy : {
			// load using script tags for cross domain, if the data in on the same domain as
			// this page, an extjs store httpproxy would be better
			type : 'ajax',
			url : 'getOcorrenciasJson.php',
			reader : {
				root : 'ocorrencias',
				totalProperty : 'total'
			},
			// sends single sort as multi parameter
			simpleSortMode : true,

			// Parameter name to send filtering information in
			filterParam : 'query',

			// The PHP script just use query=<whatever>
			encodeFilters : function(filters) {
				return filters[0].value;
			}
		},
		listeners : {
			totalcountchange : onStoreSizeChange
		},
		remoteFilter : true,
		remoteSort : true,
		autoLoad : true
	});

	function onStoreSizeChange() {
		grid.down('#status').update({
			count : store.getTotalCount()
		});
	}

	function renderTopic(value, p, record) {
		return Ext.String.format('<a href="http://sencha.com/forum/showthread.php?p={1}" target="_blank">{0}</a>', value, record.getId());
	}

	var grid = Ext.create('Ext.grid.Panel', {
		width : 700,
		height : 500,
		// collapsible : true,
		title : 'Ocorrências',
		store : store,
		loadMask : true,
		dockedItems : [{
			dock : 'top',
			xtype : 'toolbar',
			items : [{
				width : 200,
				fieldLabel : 'Pesquisa',
				labelWidth : 50,
				xtype : 'searchfield',
				store : store
			}, '->', {
				xtype : 'component',
				itemId : 'status',
				tpl : 'encontradas: {count}',
				style : 'margin-right:5px'
			}]
		}],
		selModel : {
			pruneRemoved : false
		},
		multiSelect : true,
		viewConfig : {
			trackOver : false
		},
		// grid columns
		columns : [{
			xtype : 'rownumberer',
			width : 50,
			sortable : false
		}, {
			tdCls : 'x-grid-cell-topic',
			text : "Assunto",
			dataIndex : 'titulo',
			flex : 1,
			renderer : renderTopic,
			sortable : true
		}, {
			text : "Author",
			dataIndex : 'user_name',
			width : 100,
			hidden : false,
			sortable : true
		}, {
			text : "Participação",
			dataIndex : 'participacao',
			// align : 'center',
			width : 140,
			sortable : false
		}, {
			// id : 'last',
			text : "Registada em",
			dataIndex : 'data_hora',
			xtype : 'datecolumn',
			width : 130,
			renderer : Ext.util.Format.dateRenderer('Y-m-d'),
			sortable : true
		}],
		renderTo : Ext.getBody()
	});
});
