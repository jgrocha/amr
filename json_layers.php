<?php
/*
 * curl http://softwarelivre.cm-agueda.pt/acessibilidades/json_layers.php
 * curl http://localhost/aptana/acessibilidades/json_layers.php
 * curl http://localhost/git/acessibilidades/json_layers.php
 *  */
require_once 'DB.php';
/* ligar à base de dados postgis */
$servidor = $_SERVER['SERVER_NAME'];
$pos = strpos($servidor, 'localhost');
if ($pos === false) {
	$dsn = "pgsql://geobox:geobox@amr.osgeopt.pt:5432/amr";
} else {
	$dsn = "pgsql://geobox:geobox@localhost:5432/ip";
}

$tabela = 'tema';

$db = DB::connect($dsn, false);
if (DB::isError($db)) {
	die($db -> getMessage());
}

/*
 * Pode chegar um parâmetro 'query', com um termo de pesquisa, do Ext.ux.form.SearchField
 */

if (isset($_POST['query']) && strlen(trim($_POST['query'])) > 0) {
	$pesquisa = $_POST['query'];
} else {
	$pesquisa = "";
}

if ($pesquisa != "") {
	$where .= sprintf("where titulo ilike '%%%s%%' or ", $pesquisa);
	$where .= sprintf("layer ilike '%%%s%%' or ", $pesquisa);
	$where .= sprintf("grupo ilike '%%%s%%' or ", $pesquisa);
	$where .= sprintf("regulamento ilike '%%%s%%' or ", $pesquisa);
	$where .= sprintf("observacoes ilike '%%%s%%' ", $pesquisa);
} else {
	$where = "";
}

$query = "select id, ord, titulo, layer, grupo, url, tipo, srid, estilo, qtip, ";
$query .= "CASE WHEN singletile THEN 1 ELSE 0 END as singletile, CASE WHEN activo THEN '1' ELSE '0' END as activo, ";
$query .= "regulamento, observacoes, CASE WHEN visivel THEN '1' ELSE '0' END as visivel, CASE WHEN isbaselayer THEN '1' ELSE '0' END as isbaselayer, dataalteracao ";
$query .= "from " . $tabela . " " .  $where;

$query .= "where activo = TRUE ";
$query .= "order by ord asc ";
$res = $db -> query($query);
if (DB::isError($res)) {
	die($res -> getMessage());
}
$tabela = array();
while ($row = $res -> fetchRow(DB_FETCHMODE_ASSOC)) {
	array_push($tabela, $row);
}
$result["camadas"] = $tabela;
header('Content-type: application/json');
echo json_encode($result);
$db -> disconnect();
?>
