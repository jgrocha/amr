<?php
/*
 * curl http://localhost/git/acessibilidades/json_tipo_ocorrencia.php
 *  */
require_once 'DB.php';
/* ligar à base de dados postgis */
$servidor = $_SERVER['SERVER_NAME'];
$pos = strpos($servidor, 'localhost');
if ($pos === false) {
	$dsn = "pgsql://geobox:geobox2k13@amr.osgeopt.pt:5432/amr";
} else {
	$dsn = "pgsql://geobox:geobox@localhost:5432/ip";
}
$tabela = 'amr.tipo_ocorrencia';
$db = DB::connect($dsn, false);
if (DB::isError($db)) {
	die($db -> getMessage());
}
$query = "select id_tipo_ocorrencia, CASE WHEN classe THEN 1 ELSE 0 END as classe, tipo_ocorrencia ";
$query .= "from " . $tabela;
$query .= " order by id_tipo_ocorrencia";

$res = $db -> query($query);
if (DB::isError($res)) {
	die($res -> getMessage());
}
$tabela = array();
while ($row = $res -> fetchRow(DB_FETCHMODE_ASSOC)) {
	array_push($tabela, $row);
}
$result["tipos"] = $tabela;
header('Content-type: application/json');
echo json_encode($result);
$db -> disconnect();
?>