Ext.Loader.setConfig({
    enabled: true,
    disableCaching: false,
    paths: {
        GeoExt: "/geoext2/src/GeoExt",
        Ext: "/extjs-4.1.1/src",
        'Ext.ux': "/extjs-4.1.1/examples/ux/"
    }
});