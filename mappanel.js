/*
 * Copyright (c) 2008-2012 The Open Source Geospatial Foundation
 *
 * Published under the BSD license.
 * See https://github.com/geoext/geoext2/blob/master/license.txt for the full
 * text of the license.
 */

Ext.require(['Ext.container.Viewport', 'Ext.state.Manager', 'Ext.state.CookieProvider', 'Ext.window.MessageBox', 'GeoExt.panel.Map']);

Ext.application({
	name : 'AMR-Alfa',
	launch : function() {

		Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider', {
			expires : new Date(new Date().getTime() + (1000 * 60 * 60 * 24 * 7)) //7 days from now
		}));

		OpenLayers.DOTS_PER_INCH = 25.4 / 0.28;
		WmsImageformat = 'image/png';
		
		//OpenLayers Proxy
		OpenLayers.ProxyHost = "/OpenLayers-2.12/examples/proxy.cgi?url=";

		var bounds = new OpenLayers.Bounds(-119191.40749999962, -300404.80399999936, 162129.08110000013, 276083.7674000006);
		var options = {
			maxExtent : bounds,
			// scales : [1000000, 500000, 250000, 100000, 50000, 25000, 10000, 5000, 1000, 500],
			resolutions : [279.9998488001, 139.9999244, 69.9999622, 27.99998488, 13.99999244, 6.99999622, 2.799998488, 1.399999244, 0.2799998488, 0.1399999244],
			// resolutions : [140.0, 70.0, 27.999999999999996, 13.999999999999998, 6.999999999999999, 2.8, 1.4, 0.27999999999999997, 0.13999999999999999],
			projection : "EPSG:3763",
			displayProjection : new OpenLayers.Projection("EPSG:3763"),
			title : 'Mapa',
			units : 'm',
			allOverlays : true
		};
		map = new OpenLayers.Map(options);
		
			var DeleteFeature = OpenLayers.Class(OpenLayers.Control, {
    			initialize: function(layer, options) {
        			OpenLayers.Control.prototype.initialize.apply(this, [options]);
        			this.layer = layer;
        			this.handler = new OpenLayers.Handler.Feature(
            		this, layer, {click: this.clickFeature}
        		);
    			},
    			clickFeature: function(feature) {
        			// if feature doesn't have a fid, destroy it
        			if(feature.fid == undefined) {
            			this.layer.destroyFeatures([feature]);
        			} else {
            			feature.state = OpenLayers.State.DELETE;
            			this.layer.events.triggerEvent("afterfeaturemodified", 
                		{feature: feature});
            			feature.renderIntent = "select";
            			this.layer.drawFeature(feature);
        			}
    			},
    			
    			setMap: function(map) {
        			this.handler.setMap(map);
        			OpenLayers.Control.prototype.setMap.apply(this, arguments);
    			},
    			
    			CLASS_NAME: "OpenLayers.Control.DeleteFeature"
			});


		map.addControl(new OpenLayers.Control.ScaleLine());
		map.addControl(new OpenLayers.Control.Scale());
		map.addControl(new OpenLayers.Control.Attribution());
		var posicaoRato = new OpenLayers.Control.MousePosition({
			id : "coordenadas_rato"
		});
		map.addControl(posicaoRato);
		//map.addControl(new OpenLayers.Control.PanZoomBar());
		map.addControl(new OpenLayers.Control.LayerSwitcher());
		map.addControl(new OpenLayers.Control.Permalink('permalink'));

		var wms_municipios = new OpenLayers.Layer.WMS("Municípios", "http://webgis.di.uminho.pt/geoserver/gwc/service/wms?", {
			layers : 'amr:concelhos',
			format : WmsImageformat,
			transparent : true
		}, {
			singleTile : false,
			ratio : 1,
			isBaseLayer : true,
			visibility : true,
			iconCls : "xnd-icon",
			icon : 'images/Mf_area.png',
			qtip : 'Municípios'
		});
		
		var wms_freguesias = new OpenLayers.Layer.WMS("Freguesias", "http://webgis.di.uminho.pt/geoserver/gwc/service/wms?", {
			layers : 'amr:freguesias',
			format : WmsImageformat,
			transparent : true
		}, {
			singleTile : false,
			ratio : 1,
			isBaseLayer : true,
			visibility : true,
			minScale: 100000,
			iconCls : "xnd-icon",
			icon : 'images/Mf_area.png',
			qtip : 'Freguesias'
		});

		var osm = new OpenLayers.Layer.WMS("OpenStreetMap", "http://webgis.di.uminho.pt/geoserver/gwc/service/wms?", {
			layers : 'osmpt3763',
			format : WmsImageformat,
			transparent : true
		}, {
			singleTile : false,
			ratio : 1,
			isBaseLayer : true,
			visibility : true,
			iconCls : "xnd-icon",
			icon : 'images/Mf_area.png',
			qtip : 'OpenStreetMap'
		});
		
		var wms_ocorrencias = new OpenLayers.Layer.WMS("Ocorrências", "http://webgis.di.uminho.pt/geoserver/gwc/service/wms?", {
			layers : 'amr:ocorrencias',
			format : WmsImageformat,
			transparent : true
		}, {
			singleTile : false,
			ratio : 1,
			isBaseLayer : true,
			visibility : true,
			iconCls : "xnd-icon",
			icon : 'images/Mf_area.png',
			qtip : 'Ocorrências'
		});
		 //set up a save strategy
 		var saveStrategy = new OpenLayers.Strategy.Save();
 		//saveStrategy.events.register("success", '', showSuccessMsg);
 		//saveStrategy.events.register("failure", '', showFailureMsg);
		
		var wfs_ocorrencia = new OpenLayers.Layer.Vector('Ocorrências WFS', {
             strategies: [new OpenLayers.Strategy.BBOX(), saveStrategy],
             protocol: new OpenLayers.Protocol.WFS({
	            url: 'http://webgis.di.uminho.pt/geoserver/wfs',
             	featureType: 'ocorrencias',
             	featureNS: 'http://postgis.org',
             	srsName:'EPSG:3763',	
             	version: '1.1.0',
             	reportError: true,
                featurePrefix: 'amr',
             	schema: 'http://webgis.di.uminho.pt/geoserver/wfs/DescribeFeatureType?version=1.1.0&typename=amr:ocorrencias',
             	geometryName: 'the_geom'
             	}),
             visibility: true  
        });
                
	//map.addControl(new OpenLayers.Control.EditingToolbar (wfs_ocorrencia));
	map.addLayers([wms_municipios, wms_freguesias, osm, wfs_ocorrencia, wms_ocorrencias]);
	
	// add the custom editing toolbar
	    var panel = new OpenLayers.Control.Panel({
        displayClass: 'customEditingToolbar',
        allowDepress: true
    });
    
    var draw = new OpenLayers.Control.DrawFeature(
        wfs_ocorrencia, OpenLayers.Handler.Point,
        {
            title: "Draw Feature",
            displayClass: "olControlDrawFeaturePoint",
            multi: true
        }
    );
    
    var edit = new OpenLayers.Control.ModifyFeature(wfs_ocorrencia, {
        title: "Modify Feature",
        displayClass: "olControlModifyFeature"
    });

    var del = new DeleteFeature(wfs_ocorrencia, {title: "Delete Feature"});
   
    var save = new OpenLayers.Control.Button({
        title: "Save Changes",
        trigger: function() {
            if(edit.feature) {
                edit.selectControl.unselectAll();
            }
            saveStrategy.save();
        },
        displayClass: "olControlSaveFeatures"
    });

    panel.addControls([save, del, edit, draw]);
    map.addControl(panel);


		mappanel = Ext.create('GeoExt.panel.Map', {
			title : 'Mapa suportado por OpenLayers + ExtJS + GeoExt 2',
			map : map,
			// center : '-8,40',
			center : '-48793,108614', // coordenadas EPSG:3763
			zoom : 2, // 6,
			stateful : true,
			stateId : 'mappanel',
			//            extent: '12.87,52.35,13.96,52.66',
			dockedItems : [{
				xtype : 'toolbar',
				dock : 'top',
				items : [{
					text : 'Current center of the map',
					handler : function() {
						var c = GeoExt.panel.Map.guess().map.getCenter();
						var z = GeoExt.panel.Map.guess().map.getZoom();
						console.debug(z);
						Ext.Msg.alert(this.getText(), c.toString() + ' zoom=' + z);
					}
				}]
			}]
		});

		Ext.create('Ext.container.Viewport', {
			layout : 'fit',
			items : [mappanel]
		});
	}
}); 