/*
 * Copyright (c) 2008-2012 The Open Source Geospatial Foundation
 *
 * Published under the BSD license.
 * See https://github.com/geoext/geoext2/blob/master/license.txt for the full
 * text of the license.
 */

Ext.require(['Ext.container.Viewport', 'Ext.state.Manager', 'Ext.state.CookieProvider', 'Ext.window.MessageBox', 'GeoExt.panel.Map', 'GeoExt.window.Popup']);

Ext.application({
	name : 'AMR-Alfa',
	launch : function() {

		Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider', {
			expires : new Date(new Date().getTime() + (1000 * 60 * 60 * 24 * 7)) //7 days from now
		}));

		OpenLayers.DOTS_PER_INCH = 25.4 / 0.28;
		WmsImageformat = 'image/png';

		//OpenLayers Proxy
		OpenLayers.ProxyHost = "/OpenLayers-2.12/examples/proxy.cgi?url=";

		var bounds = new OpenLayers.Bounds(-119191.40749999962, -300404.80399999936, 162129.08110000013, 276083.7674000006);
		var options = {
			maxExtent : bounds,
			// scales : [1000000, 500000, 250000, 100000, 50000, 25000, 10000, 5000, 1000, 500],
			resolutions : [279.9998488001, 139.9999244, 69.9999622, 27.99998488, 13.99999244, 6.99999622, 2.799998488, 1.399999244, 0.2799998488, 0.1399999244],
			// resolutions : [140.0, 70.0, 27.999999999999996, 13.999999999999998, 6.999999999999999, 2.8, 1.4, 0.27999999999999997, 0.13999999999999999],
			projection : "EPSG:3763",
			displayProjection : new OpenLayers.Projection("EPSG:3763"),
			title : 'Mapa',
			units : 'm',
			allOverlays : true
		};
		map = new OpenLayers.Map(options);

		map.addControl(new OpenLayers.Control.ScaleLine());
		map.addControl(new OpenLayers.Control.Scale());
		map.addControl(new OpenLayers.Control.Attribution());
		var posicaoRato = new OpenLayers.Control.MousePosition({
			id : "coordenadas_rato"
		});
		map.addControl(posicaoRato);
		//map.addControl(new OpenLayers.Control.PanZoomBar());
		map.addControl(new OpenLayers.Control.LayerSwitcher());
		map.addControl(new OpenLayers.Control.Permalink('permalink'));

		var wms_municipios = new OpenLayers.Layer.WMS("Municípios", "http://webgis.di.uminho.pt/geoserver/gwc/service/wms?", {
			layers : 'amr:concelhos',
			format : WmsImageformat,
			transparent : true
		}, {
			singleTile : false,
			ratio : 1,
			isBaseLayer : true,
			visibility : true,
			iconCls : "xnd-icon",
			icon : 'images/Mf_area.png',
			qtip : 'Municípios'
		});

		var wms_freguesias = new OpenLayers.Layer.WMS("Freguesias", "http://webgis.di.uminho.pt/geoserver/gwc/service/wms?", {
			layers : 'amr:freguesias',
			format : WmsImageformat,
			transparent : true
		}, {
			singleTile : false,
			ratio : 1,
			isBaseLayer : true,
			visibility : true,
			minScale : 100000,
			iconCls : "xnd-icon",
			icon : 'images/Mf_area.png',
			qtip : 'Freguesias'
		});

		var osm = new OpenLayers.Layer.WMS("OpenStreetMap", "http://webgis.di.uminho.pt/geoserver/gwc/service/wms?", {
			layers : 'osmpt3763',
			format : WmsImageformat,
			transparent : true
		}, {
			singleTile : false,
			ratio : 1,
			isBaseLayer : true,
			visibility : true,
			iconCls : "xnd-icon",
			icon : 'images/Mf_area.png',
			qtip : 'OpenStreetMap'
		});

		var context = {
			getColor : function(feature) {
				var cor;
				// console.debug(feature);
				switch(parseInt(feature.attributes["id_tipo_ocorrencia"])) {
					case 1:
						cor = 'Red';
						break;
					// #FF0000
					case 2:
						cor = 'Olive';
						break;
					// #808000
					case 3:
						cor = 'OliveDrab';
						break;
					// #6B8E23
					case 4:
						cor = 'Orange';
						break;
					// #FFA500
					case 5:
						cor = 'OrangeRed';
						break;
					// #FF4500
					case 6:
						cor = 'Orchid';
						break;
					// #DA70D6
					case 7:
						cor = 'PaleGoldenRod';
						break;
					// #EEE8AA
					case 8:
						cor = 'PaleGreen';
						break;
					// #98FB98
					case 9:
						cor = 'PaleTurquoise';
						break;
					// #AFEEEE
					case 10:
						cor = 'PaleVioletRed';
						break;
					// #DB7093
					case 11:
						cor = 'PapayaWhip';
						break;
					// #FFEFD5
					case 12:
						cor = 'PeachPuff';
						break;
					// #FFDAB9
					case 13:
						cor = 'Peru';
						break;
					// #CD853F
					case 14:
						cor = 'Pink';
						break;
					// #FFC0CB
					case 15:
						cor = 'Plum';
						break;
					// #DDA0DD
					case 16:
						cor = 'PowderBlue';
						break;
					// #B0E0E6
					case 17:
						cor = 'Purple';
						break;
					// #800080
					default:
						cor = 'gray';
				}
				return cor;
			}
		};
		var template = {
			cursor : "pointer",
			fillOpacity : 0.5,
			fillColor : "${getColor}",
			pointRadius : 5,
			strokeWidth : 1,
			strokeOpacity : 1,
			strokeColor : "${getColor}",
			graphicName : "triangle"
		};
		var style = new OpenLayers.Style(template, {
			context : context
		});

		var context_2 = {
			getColor : function(feature) {
				var cor_2;
				// console.debug(feature);
				switch(parseInt(feature.attributes["id_estado"])) {
					case 1:
						cor_2 = 'Red';
						break;
					// #FF0000
					case 2:
						cor_2 = 'PaleGreen';
						break;
					// #98FB98
					default:
						cor_2 = 'gray';
				}
				return cor_2;
			}
		};
		var template_estado = {
			cursor : "pointer",
			fillOpacity : 0.5,
			fillColor : "${getColor}",
			pointRadius : 5,
			strokeWidth : 1,
			strokeOpacity : 1,
			strokeColor : "${getColor}",
			graphicName : "square"
		};
		var style_estado = new OpenLayers.Style(template_estado, {
			context : context_2
		});

		var wfs_ocorrencia = new OpenLayers.Layer.Vector('Participações', {
			strategies : [new OpenLayers.Strategy.BBOX()],
			styleMap : new OpenLayers.StyleMap({
				'default' : style_estado
			}),
			protocol : new OpenLayers.Protocol.WFS({
				url : 'http://webgis.di.uminho.pt/geoserver/wfs',
				featureType : 'ocorrencias',
				featureNS : 'http://postgis.org',
				srsName : 'EPSG:3763',
				version : '1.1.0',
				reportError : true,
				featurePrefix : 'amr',
				schema : 'http://webgis.di.uminho.pt/geoserver/wfs/DescribeFeatureType?version=1.1.0&typename=amr:ocorrencias',
				geometryName : 'the_geom'
			}),
			visibility : true
		});

		map.addLayers([wms_municipios, wms_freguesias, osm, wfs_ocorrencia]);

		var mostraDados = function(e) {
			createPopup(e.feature);
		};
		var limpaDados = function(e) {
			// removePopup();
			var comWin = Ext.getCmp('pipocas');
			comWin.close();
		};

		var highlightCtrl = new OpenLayers.Control.SelectFeature(wfs_ocorrencia, {
			hover : true,
			highlightOnly : true,
			eventListeners : {
				beforefeaturehighlighted : mostraDados,
				featureunhighlighted : limpaDados
			}
		});
		map.addControl(highlightCtrl);
		highlightCtrl.activate();

		function createPopup(feature) {
			// console.debug(feature);
			// todos os features sacados com o WFS têm um feature.attributes["id_ocorrencia"]
			// todos os features sacados com o WFS têm um feature.fid
			// no entanto, os novos, criados com o OpenLayers, não têm feature.attributes["id_ocorrencia"]
			// têm APENAS feature.fid, que é atribuído quando são gravados
			popup = Ext.create('GeoExt.window.Popup', {
				id : 'pipocas',
				title : feature.attributes["titulo"],
				location : feature,
				width : 200,
				html : feature.attributes["participacao"],
				maximizable : true,
				collapsible : true,
				anchorPosition : 'auto',
				loader : {
					url : 'getOcorrencia.php',
					scripts : true,
					autoLoad : true,
					params : {
						fid : feature.fid, // feature.attributes["id_ocorrencia"],
						user_name : 'amr'
					},
				}
			});
			// unselect feature when the popup
			// is closed
			popup.on({
				close : function() {
					if (OpenLayers.Util.indexOf(wfs_ocorrencia.selectedFeatures, this.feature) > -1) {
						selectCtrl.unselect(this.feature);
					}
				}
			});
			// console.log('Vai carregar a ocorrência ' + feature.attributes["id_ocorrencia"]);
			// popup.getLoader().load();
			popup.show();
		}

		mappanel = Ext.create('GeoExt.panel.Map', {
			title : 'Mapa suportado por OpenLayers + ExtJS + GeoExt 2',
			map : map,
			// center : '-8,40',
			center : '-48793,108614', // coordenadas EPSG:3763
			zoom : 2, // 6,
			stateful : true,
			stateId : 'mappanel',
			//            extent: '12.87,52.35,13.96,52.66',
			dockedItems : [{
				xtype : 'toolbar',
				dock : 'top',
				items : [{
					text : 'Current center of the map',
					handler : function() {
						var c = GeoExt.panel.Map.guess().map.getCenter();
						var z = GeoExt.panel.Map.guess().map.getZoom();
						console.debug(z);
						Ext.Msg.alert(this.getText(), c.toString() + ' zoom=' + z);
					}
				}, {
					text : 'Style Estado',
					handler : function() {
						wfs_ocorrencia.styleMap.styles["default"] = style_estado;
						wfs_ocorrencia.redraw();
					}
				}, {
					text : 'Style Ocorrências',
					handler : function() {
						wfs_ocorrencia.styleMap.styles["default"] = style;
						wfs_ocorrencia.redraw();
					}
				}]
			}]
		});

		var w = Ext.create('Ext.Window', {
			title : 'Ocorrências...',
			width : 400,
			height : 200,
			closable : false,
			x : 10,
			y : 450,
			plain : true,
			headerPosition : 'bottom',
			layout : 'fit',
			items : {
				border : false
			}
		});
		w.show();
		w.alignTo(Ext.getBody(), "tr-tr", [-10, 10]);

		Ext.create('Ext.container.Viewport', {
			layout : 'fit',
			items : [mappanel],
			monitorResize : true,
			listeners : {
				resize : {
					fn : function(el) {
						// x, y
						w.alignTo(Ext.getBody(), "br-br", [-10, -80]);
						// console.log('panel resize');
					}
				}
			}
		});
	}
});
