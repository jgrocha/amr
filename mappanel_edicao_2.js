/*
 * A Minha Rua
 */

Ext.require(['Ext.container.Viewport', 'Ext.state.Manager', 'Ext.TaskManager', 'Ext.form.field.File', 'Ext.form.field.ComboBox', 'Ext.form.Panel', 'Ext.state.CookieProvider', 'Ext.window.MessageBox', 'Ext.form.field.Hidden']);
Ext.require(['Ext.grid.*', 'Ext.data.*', 'Ext.util.*', 'Ext.grid.PagingScroller', 'Ext.ux.form.SearchField']);
Ext.require(['Ext.data.reader.Json', 'Ext.layout.container.Border', 'Ext.toolbar.Spacer', 'Ext.layout.container.Accordion'])
Ext.require(['GeoExt.panel.Map', 'GeoExt.data.FeatureStore', 'GeoExt.grid.column.Symbolizer', 'GeoExt.selection.FeatureModel', 'GeoExt.window.Popup', 'GeoExt.form.field.GeocoderComboBox', 'GeoExt.tree.Panel', 'GeoExt.tree.OverlayLayerContainer', 'GeoExt.tree.BaseLayerContainer', 'GeoExt.tree.LayerContainer', 'GeoExt.data.LayerTreeModel', 'GeoExt.tree.View', 'GeoExt.tree.Column', 'GeoExt.container.WmsLegend']);

Ext.application({
	name : 'AMR',
	launch : function() {

		Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider', {
			expires : new Date(new Date().getTime() + (1000 * 60 * 60 * 24 * 7))
			// 7 days from now
		}));

		OpenLayers.DOTS_PER_INCH = 25.4 / 0.28;
		WmsImageformat = 'image/png';
		format = 'image/png';

		// OpenLayers Proxy
		if (document.location.hostname == "localhost") {
			OpenLayers.ProxyHost = "/OpenLayers-2.13.1/examples/proxy.cgi?url=";
		}

		var bounds = new OpenLayers.Bounds(-119191.40749999962, -300404.80399999936, 162129.08110000013, 276083.7674000006);
		var options = {
			maxExtent : bounds,
			// scales : [1000000, 500000, 250000, 100000, 50000, 25000,
			// 10000, 5000, 1000, 500],
			resolutions : [279.9998488001, 139.9999244, 69.9999622, 27.99998488, 13.99999244, 6.99999622, 2.799998488, 1.399999244, 0.2799998488, 0.1399999244],
			// resolutions : [140.0, 70.0, 27.999999999999996,
			// 13.999999999999998, 6.999999999999999, 2.8, 1.4,
			// 0.27999999999999997, 0.13999999999999999],
			projection : new OpenLayers.Projection("EPSG:3763"),
			displayProjection : new OpenLayers.Projection("EPSG:3763"),
			title : 'Mapa',
			units : 'm',
			allOverlays : false // por causa da tree
		};
		map = new OpenLayers.Map(options);

		map.addControl(new OpenLayers.Control.ScaleLine());
		map.addControl(new OpenLayers.Control.Scale());
		map.addControl(new OpenLayers.Control.Attribution());
		var posicaoRato = new OpenLayers.Control.MousePosition({
			id : "coordenadas_rato"
		});
		map.addControl(posicaoRato);
		// map.addControl(new OpenLayers.Control.PanZoomBar());
		// map.addControl(new OpenLayers.Control.LayerSwitcher());
		map.addControl(new OpenLayers.Control.Permalink('permalink'));

		Ext.define('GeoLayer', {
			extend : 'Ext.data.Model',
			fields : [{
				name : 'id',
				type : 'int'
			}, {
				name : 'ord',
				type : 'int'
			}, {
				name : 'titulo',
				type : 'string'
			}, {
				name : 'layer',
				type : 'string'
			}, {
				name : 'grupo',
				type : 'string'
			}, {
				name : 'url',
				type : 'string'
			}, {
				name : 'tipo',
				type : 'string'
			}, {
				name : 'srid',
				type : 'int'
			}, {
				name : 'estilo',
				type : 'string'
			}, {
				name : 'qtip',
				type : 'string'
			}, {
				name : 'singletile',
				type : 'boolean'
			}, {
				name : 'activo',
				type : 'boolean'
			}, {
				name : 'regulamento',
				type : 'string'
			}, {
				name : 'observacoes',
				type : 'string'
			}, {
				name : 'visivel',
				type : 'boolean'
			}, {
				name : 'isbaselayer',
				type : 'boolean'
			}, {
				name : 'dataalteracao',
				type : 'date',
				dateFormat : 'c'	// impecável: a data vem no formato ISO8601
			}],
			idProperty : 'id'
		});

		var storeLayers = new Ext.data.JsonStore({
			model : 'GeoLayer',
			proxy : {
				type : 'ajax',
				url : 'json_layers.php',
				reader : {
					type : 'json',
					root : 'camadas'
					// totalProperty: 'total'
				}
			},
			listeners : {
				// invocado depois de ler os dados
				'load' : function() {
					storeLayers.each(function(r) {
						tip = '';
						if (r.data['qtip'] == '') {
							tipURL = '/geoserver/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&LAYER=' + r.data['layer'] + '&legend_options=forceLabels:on';
							tip = r.data['titulo'] + '<br/>' + '<img src="' + tipURL + '" alt="' + r.data['titulo'] + '"></img>';
						} else {
							tip = r.data['qtip'];
						}
						map.addLayer(new OpenLayers.Layer.WMS(r.data['titulo'], OpenLayers.ProxyHost + r.data['url'], {
							layers : r.data['layer'],
							format : WmsImageformat,
							transparent : true
						}, {
							singleTile : r.data['singletile'],
							ratio : 1,
							isBaseLayer : r.data['isbaselayer'],
							visibility : r.data['visivel'],
							group : r.data['grupo'],
							iconCls : "xnd-icon",
							icon : r.data['tipo'],
							qtip : tip
						}));
						console.log('Adicionado o layer ' + r.data['layer']);
					});
				}
			}
		});
		storeLayers.load();

		var osm = new OpenLayers.Layer.WMS("OpenStreetMap", "http://webgis.di.uminho.pt/geoserver/gwc/service/wms?", {
			layers : 'osmpt3763',
			format : WmsImageformat,
			transparent : true,
			displayInLayerSwitcher : true
		}, {
			singleTile : false,
			ratio : 1,
			isBaseLayer : true,
			visibility : true,
			iconCls : "xnd-icon",
			icon : 'images/Mf_area.png',
			qtip : 'OpenStreetMap'
		});

		var contextTipoOcorrencia = {
			getExternalGraphic : function(feature) {
				var externalGraphic;
				// console.debug(feature);
				var tipo = parseInt(feature.attributes["id_tipo_ocorrencia"]);
				switch (tipo) {
					case 1:
						// 1;"Rampeamento e rebaixamento";TRUE
						externalGraphic = 'images/traffic-cone-icon-blue-32.png';
						// 'simbologia/SVG/i223.svg';
						break;
					case 10:
					// 10;"Passeios";TRUE
					case 11:
					// 11;"Estacionamento abusivo no passeio";FALSE
					case 12:
					// 12;"Esplanadas";FALSE
					case 13:
					// 13;"Contentor RSU";FALSE
					case 14:
					// 14;"Tapumes de obras";FALSE
					case 15:
						// 15;"Buracos no passeio";FALSE
						externalGraphic = 'images/traffic-cone-icon-roxo-32.png';
						// 'simbologia/SVG/EstacAbusivo_EA.svg';
						break;
					case 20:
					// 20;"Mobiliário Urbano";TRUE
					case 21:
					// 21;"Candeeiros";FALSE
					case 22:
					// 22;"Bancos";FALSE
					case 23:
					// 23;"Armarios (Comunic., Eletric., Gás)";FALSE
					case 24:
					// 24;"Hidrantes";FALSE
					case 25:
					// 25;"Marco Correio";FALSE
					case 26:
					// 26;"Mupi";FALSE
					case 27:
					// 27;"Obstáculo comercial";FALSE
					case 28:
					// 28;"Cabina telefónica";FALSE
					case 29:
					// 29;"Fontes e bebedouros";FALSE
					case 30:
					// 30;"Sinalizaçao vertical";FALSE
					case 31:
					// 31;"Papeleiras";FALSE
					case 32:
					// 32;"Pilaretes";FALSE
					case 33:
					// 33;"Gradeamento";FALSE
					case 34:
					// 34;"Placas toponímicas";FALSE
					case 35:
						// 35;"Abrigo de transporte público";FALSE
						externalGraphic = 'images/traffic-cone-icon-red-32.png';
						// 'simbologia/SVG/Papeleira.svg';
						break;
					case 40:
					// 40;"Elementos da vegetação";TRUE
					case 41:
					// 41;"Árvores";FALSE
					case 42:
					// 42;"Caldeiras";FALSE
					case 43:
						// 43;"Floreiras";FALSE
						externalGraphic = 'images/traffic-cone-icon-green-32.png';
						// 'simbologia/SVG/EstacAbusivo_EA.svg';
						break;
					case 50:
					// 50;"Outras situações";TRUE
					default:
						externalGraphic = 'images/traffic-cone-icon-yellow-32.png';
					// 'simbologia/SVG/OutrosObst.svg';
				}
				return externalGraphic;
			}
		};
		var templateTipoOcorrencia = {
			graphicWidth : 32,
			graphicHeight : 32,
			// graphicYOffset : -15,
			graphicOpacity : 1,
			cursor : "pointer",
			externalGraphic : "${getExternalGraphic}"
		};

		var styleTipoOcorrencia = new OpenLayers.Style(templateTipoOcorrencia, {
			context : contextTipoOcorrencia
		});

		var context_2 = {
			getColor : function(feature) {
				var cor_2;
				// console.debug(feature);
				switch (parseInt(feature.attributes["id_estado"])) {
					case 1:
						cor_2 = 'Red';
						break;
					// #FF0000
					case 2:
						cor_2 = 'PaleGreen';
						break;
					// #98FB98
					default:
						cor_2 = 'gray';
				}
				return cor_2;
			}
		};

		var defStyle = {
			externalGraphic : "images/Map-Marker-Flag-Green-32x32.png",
			graphicWidth : 32,
			graphicHeight : 32,
			graphicYOffset : -15,
			graphicOpacity : 1,
			cursor : "pointer"
		};

		var template_participacao = {
			strokeColor : "${getColor}",
			externalGraphic : 'images/Map-Marker-Flag-Green-32x32.png'
		};
		var style_estado = new OpenLayers.Style(defStyle, {
			context : context_2
		});

		var saveStrategy = new OpenLayers.Strategy.Save();
		saveStrategy.events.register('success', null, saveSuccess);
		saveStrategy.events.register('fail', null, saveFail);

		var wfs_ocorrencia;
		if (document.location.hostname == "localhost") {
			wfs_ocorrencia = new OpenLayers.Layer.Vector('Participações', {
				strategies : [new OpenLayers.Strategy.BBOX(), saveStrategy],
				styleMap : new OpenLayers.StyleMap({
					'default' : styleTipoOcorrencia // style_estado
				}),
				protocol : new OpenLayers.Protocol.WFS({
					url : 'http://localhost/geoserver/wfs',
					featureType : 'ocorrencias',
					featureNS : 'http://www.openplans.org/topp',
					srsName : 'EPSG:3763',
					version : '1.1.0',
					reportError : true,
					featurePrefix : 'amr',
					schema : 'http://localhost/geoserver/wfs/DescribeFeatureType?version=1.1.0&typename=topp:ocorrencias',
					geometryName : 'the_geom'
				}),
				visibility : true,
				projection : new OpenLayers.Projection("EPSG:3763")
			});
		} else {
			wfs_ocorrencia = new OpenLayers.Layer.Vector('Participações', {
				strategies : [new OpenLayers.Strategy.BBOX(), saveStrategy],
				styleMap : new OpenLayers.StyleMap({
					'default' : styleTipoOcorrencia // style_estado
				}),
				protocol : new OpenLayers.Protocol.WFS({
					url : 'http://amr.osgeopt.pt/geoserver/wfs',
					featureType : 'ocorrencias',
					featureNS : 'http://postgis.org', //
					srsName : 'EPSG:3763',
					version : '1.1.0',
					reportError : true,
					featurePrefix : 'amr',
					schema : 'http://amr.osgeopt.pt/geoserver/wfs/DescribeFeatureType?version=1.1.0&typename=amr:ocorrencias',
					geometryName : 'the_geom'
				}),
				visibility : true,
				projection : new OpenLayers.Projection("EPSG:3763")
			});
		}

		// map.addLayers([wms_municipios, wms_freguesias, osm, wfs_ocorrencia]);
		map.addLayers([osm, wfs_ocorrencia]);
		// map.addLayer(wfs_ocorrencia);

		/*
		 var route_layer = new OpenLayers.Layer.Vector("Pesquisa", {
		 displayInLayerSwitcher : false,
		 styleMap : new OpenLayers.StyleMap(new OpenLayers.Style({
		 strokeColor : "#ff9933",
		 strokeWidth : 3
		 })),
		 protocol : new OpenLayers.Protocol.HTTP({
		 url : 'json_procura_osm.php',
		 format : new OpenLayers.Format.GeoJSON()
		 })
		 });
		 map.addLayer(route_layer);
		 */

		var locationLayer = new OpenLayers.Layer.Vector("Location", {
			projection : new OpenLayers.Projection("EPSG:3763"),
			styleMap : new OpenLayers.Style({
				externalGraphic : "http://openlayers.org/api/img/marker.png",
				graphicYOffset : -25,
				graphicHeight : 25,
				graphicTitle : "${name}"
			})
		});

		map.addLayer(locationLayer);

		var mostraDados = function(e) {
			createPopup(e.feature);
			// como o feature ficava selecionado, não acontecia o evento featurehighlighted novamente
			highlightCtrl.unselectAll();
			console.log('popup criado');
		};

		var limpaDados = function(e) {
			// removePopup();
			console.log('vai remover popup');
			var comWin = Ext.getCmp('pipocas');
			comWin.close();
		};

		var highlightCtrl = new OpenLayers.Control.SelectFeature(wfs_ocorrencia, {
			// hover : true,
			// clickout : true, // {Boolean} Unselect features when clicking outside any feature.  Default is true.
			highlightOnly : true,
			eventListeners : {
				beforefeaturehighlighted : mostraDados
				// featurehighlighted : mostraDados
				// featureunhighlighted : limpaDados
			}
		});

		var submeteComentario = function(event) {
			console.log('submeteComentario');
			console.debug(event.currentTarget);
			// var form = button.up('form').getForm();
			/*
			 var form = formComentario;
			 if (form.isValid()) {
			 console.log('Vai fazer submit do comentário...');
			 form.submit({
			 url : 'submitComentario.php',
			 waitMsg : 'A registar comentário...',
			 submitEmptyText : false, // para não submeter o "Escreva um comentário..."
			 success : function(formulario, acao) {
			 msg('Success', 'O seu comentário foi registado com sucesso com apenas um ENTER.');
			 button.up('.window').close();
			 }
			 });
			 }
			 */
		};

		function createPopup(feature) {
			// console.debug(feature);
			// todos os features sacados com o WFS têm um feature.attributes["id_ocorrencia"]
			// todos os features sacados com o WFS têm um feature.fid
			// no entanto, os novos, criados com o OpenLayers, não têm feature.attributes["id_ocorrencia"]
			// têm APENAS feature.fid, que é atribuído quando são gravados

			var formComentario = Ext.create('Ext.form.Panel', {
				// renderTo: 'fi-form',
				// width : 500,
				id : 'formComentario',
				region : 'center',
				// frame : true,
				// title : 'Comentar',
				// bodyPadding : '10 10 0',
				autoWidth : true,
				autoHeight : true,
				defaults : {
					anchor : '100%',
					allowBlank : false,
					msgTarget : 'side',
					labelWidth : 0
				},
				items : [{
					xtype : 'hidden',
					name : 'fid'
				}, {
					xtype : 'hidden',
					name : 'user_name'
				}, {
					xtype : 'hidden',
					name : 'email'
				}, {
					xtype : 'hidden',
					name : 'userid'
				}, {
					xtype : 'textareafield',
					grow : true,
					name : 'comentario',
					emptyText : 'Atualização...',
					allowBlank : false
				}, {
					xtype : 'checkboxfield',
					name : 'resolvido',
					boxLabel : 'O problema está resolvido',
					inputValue : 'true',
					uncheckedValue : 'false',
					listeners : {
						change : function(f, new_val) {
							console.log('campo alterado!' + new_val);
							var form = this.up('form').getForm();
							if (new_val) {
								form.findField('comentario').allowBlank = true;
								form.findField('comentario').clearInvalid();
							} else {
								form.findField('comentario').allowBlank = false;
							}
						}
					}
				}],
				listeners : {
					afterRender : function(thisForm, options) {
						this.keyNav = Ext.create('Ext.util.KeyNav', this.el, {
							enter : submeteComentario,
							scope : this
						});
					}
				},
				buttons : [{
					text : 'Atualizar',
					handler : function(button, e) {
						var form = this.up('form').getForm();
						if (form.isValid()) {
							if (fb_nome_proprio != '') {
								form.findField('user_name').setValue(fb_nome_proprio);
								form.findField('email').setValue(fb_email);
								form.findField('userid').setValue(fb_id);
							}
							console.log('Vai fazer submit do comentário...');
							form.submit({
								url : 'submitComentario.php',
								waitMsg : 'A registar comentário...',
								submitEmptyText : false, // para não submeter o "Escreva um comentário..."
								success : function(formulario, acao) {
									msg('Success', 'O seu comentário foi registado com sucesso.');
									button.up('.window').close();
								}
							});
						}
					}
				}, {
					xtype : 'tbfill'
				}, {
					text : 'Fechar',
					handler : function(button, e) {
						button.up('.window').close();
					}
				}]
			});

			var popup = Ext.create('GeoExt.window.Popup', {
				id : 'pipocas',
				title : feature.attributes["titulo"],
				location : feature,
				width : 300,
				height : 250,
				// layout : 'fit',
				autoScroll : true,
				modal : true,
				closeAction : 'destroy',
				// html : feature.attributes["participacao"],
				maximizable : false,
				collapsible : false,
				anchorPosition : 'auto',
				type : 'vbox',
				align : 'stretch',
				// autowidth : true,
				// autoheight : true,
				items : [{
					// title : 'Bogus Tab',
					// html : feature.attributes["participacao"] ,
					xtype : 'panel',
					flex : 1,
					// layout : 'fit',
					// title : 'Mi Documentacion',
					header : false,
					autowidth : true,
					autoheight : true,
					border : true, // depois false
					// autoheight : true,
					loader : {
						url : 'getOcorrencia.php',
						renderer : 'html', // ?
						scripts : true, // a true se queremos que execute JS dentro do HTML lido
						autoLoad : true,
						params : {
							fid : feature.fid // feature.attributes["id_ocorrencia"],
							// user_name : 'amr' // não é usado!
						},
						mask : {
							msg : 'A carregar a participação...'
						},
						listeners : {
							beforeload : function(loader) {
								console.log('beforeload getOcorrencia.php');
							}
						}
					},
					listeners : {
						afterrender : function(c) {
							console.log('afterrender getOcorrencia.php');
							c.doLayout();
							// c.getLoader().startAutoRefresh(5000); // ok! - dá para refrescar automaticamente o painel, caso tenham entrado comentários entretanto
						}
					}
				}, formComentario],
			});

			// unselect feature when the popup
			// is closed
			popup.on({
				afterrender : function() {
					console.log('Preencher o form dos comentários com os campos escondidos, nomeadamente ' + feature.fid + ' com a ocorrencia resolvida? ' + feature.attributes["id_estado"]);
					// console.debug(this.down('form'));
					// this.down('form').getForm().findField('fid').setValue(feature.fid);
					var form = this.down('form').getForm();
					form.findField('fid').setValue(feature.fid);
					// form.findField('user_name').setValue('amr');
					form.findField('comentario').focus(false, 200);
					var comprimento = 20;
					// set to the number of characters you want to keep
					var titulo = feature.attributes["titulo"];
					if (titulo.length > comprimento)
						tituloCurto = titulo.substring(0, comprimento) + '(...)';
					else
						tituloCurto = titulo;
					this.setTitle(tituloCurto);
					if (feature.attributes["id_estado"] == 2) {
						this.remove('formComentario',true);
					}					
				},
				close : function() {
					console.log('destroi tudo!');
					// console.log(OpenLayers.Util.indexOf(wfs_ocorrencia.selectedFeatures, this.feature));
				},
				beforedestroy : function() {
					console.log('before destroy');
				}
			});
			popup.show();
		}

		// map.addControl(highlightCtrl);
		// highlightCtrl.activate();

		// var navControl = new OpenLayers.Control.Navigation();
		var modifyControl = new OpenLayers.Control.ModifyFeature(wfs_ocorrencia);
		var insertControl = new OpenLayers.Control.DrawFeature(wfs_ocorrencia, OpenLayers.Handler.Point, {
			'displayClass' : 'olControlDrawFeaturePoint'
		});

		// var panelControls = [navControl, highlightCtrl, modifyControl, insertControl];
		var panelControls = [highlightCtrl, /* modifyControl, */insertControl];

		var toolbar = new OpenLayers.Control.Panel({
			displayClass : 'customEditingToolbar',
			defaultControl : panelControls[0]
		});

		// toolbar.addControls(panelControls);
		// map.addControl(toolbar);

		toolbar.addControls([insertControl]);
		map.addControl(toolbar);
		map.addControl(highlightCtrl);
		highlightCtrl.activate();

		// var ultimoFeatureInserido = null;

		insertControl.events.on({
			featureadded : function(event) {
				// featureadded : function(event) {
				ultimoFeatureInserido = event.feature;
				// var geometry = f.geometry;formOcorrencia
				// console.debug(ultimoFeatureInserido);

				ultimoFeatureInserido.attributes["titulo"] = 'Acabadinho de Inserir';
				console.log('Vou ler os atributos numa janela para o feature.id = ' + ultimoFeatureInserido.id + ' com o estado ' + ultimoFeatureInserido.state);

				// windowOcorrencia.show();
				// windowOcorrencia.alignTo(Ext.getBody(), "bl-bl", [10, -10]);
				createPopupNewFeature(ultimoFeatureInserido);

				/*
				 console.log('Ui... só devia aparecer depois de o form ser fechado... ou não?');
				 console.debug(formOcorrencia);
				 console.debug(formOcorrencia.getForm());
				 console.debug(formOcorrencia.getForm().findField('id_tipo_ocorrencia').value);
				 */

				/*
				 formImagemOcorrencia.disable();
				 formOcorrencia.enable();
				 console.log('Limpar forms...');
				 formOcorrencia.getForm().reset();
				 formImagemOcorrencia.getForm().reset();
				 */
			}
		});
		Ext.define('TipoOcorrencia', {
			extend : 'Ext.data.Model',
			fields : [{
				name : 'id_tipo_ocorrencia',
				type : 'int'
			}, {
				name : 'classe',
				type : 'boolean'
			}, {
				name : 'tipo_ocorrencia',
				type : 'string'
			}]
		});

		var storeTipoOcorrencia = new Ext.data.JsonStore({
			model : 'TipoOcorrencia',
			proxy : {
				type : 'ajax',
				url : 'json_tipo_ocorrencia.php',
				reader : {
					type : 'json',
					root : 'tipos'
					// totalProperty: 'total'
				}
			},
			autoLoad : true
		});

		// Custom rendering Template
		var resultTpl = new Ext.XTemplate('<tpl for="."><div class="combo-superior-{classe}"><span>{tipo_ocorrencia}</span></div></tpl>');

		var msg = function(title, msg) {
			Ext.Msg.show({
				title : title,
				msg : msg,
				minWidth : 200,
				modal : true,
				icon : Ext.Msg.INFO,
				buttons : Ext.Msg.OK
			});
		};

		function saveSuccess(event) {
			// só agora tenho o fid atribuído... fixe, que é para ser atribuído à imagem.
			console.log('Your mapped field(s) have been successfully saved, em particular ' + ultimoFeatureInserido.fid);
			console.log('Atualizar o store JSON com as contribuições...');
			storeContribuicoesJson.load();
			console.debug(event.response);
		}

		function saveFail(event) {
			console.log('Error! Your changes could not be saved. ');
			console.debug(event.response);
			// alert('Error! Your changes could not be saved. ');
		}

		var required = '<span style="color:red;font-weight:bold" data-qtip="Obrigatório">*</span>';

		function createPopupNewFeature(feature) {

			var formOcorrencia = Ext.create('Ext.form.Panel', {
				// renderTo: 'fi-form',
				// width : 500,
				region : 'center',
				frame : true,
				title : 'Descreva o seu contributo',
				// bodyPadding : '10 10 0',
				autoWidth : true,
				autoHeight : true,
				defaults : {
					anchor : '100%',
					allowBlank : false,
					msgTarget : 'side',
					labelWidth : 50
				},
				defaultType : 'textfield',
				items : [{
					fieldLabel : 'Assunto',
					name : 'titulo',
					allowBlank : false,
					minLength : 5,
					afterLabelTextTpl : required
				}, {
					fieldLabel : 'Tipo',
					name : 'id_tipo_ocorrencia',
					xtype : 'combo',
					queryMode : 'local',
					// width : 280,
					emptyText : 'Escolha o tipo de contribuição',
					store : storeTipoOcorrencia,
					displayField : 'tipo_ocorrencia',
					valueField : 'id_tipo_ocorrencia',
					editable : false,
					listConfig : {
						itemTpl : '<tpl for="."><div class="combo-superior-{classe}"><span>{tipo_ocorrencia}</span></div></tpl>'
					},
					afterLabelTextTpl : required
					// tpl : resultTpl
				}, {
					xtype : 'textareafield',
					grow : true,
					name : 'participacao',
					fieldLabel : 'Descrição',
					anchor : '100%',
					afterLabelTextTpl : required
				}, {
					xtype : 'hidden', // 'textfield',
					name : 'user_name',
					fieldLabel : 'Nome'
					// allowBlank : false, // requires a non-empty value
					// afterLabelTextTpl : required
				}, {
					xtype : 'hidden', // 'textfield',
					name : 'email',
					fieldLabel : 'Email'
					// vtype : 'email', // requires value to be a valid email address format
					// afterLabelTextTpl : required
				}, {
					xtype : 'hidden', // 'textfield',
					name : 'userid'
				}],

				buttons : [{
					text : 'Contribuir',
					handler : function(button, e) {
						var form = this.up('form').getForm();
						if (form.isValid()) {
							formImagemOcorrencia.enable();
							formOcorrencia.disable();
							console.debug(ultimoFeatureInserido);
							ultimoFeatureInserido.attributes["titulo"] = form.findField('titulo').value;
							ultimoFeatureInserido.attributes["id_tipo_ocorrencia"] = form.findField('id_tipo_ocorrencia').value;
							ultimoFeatureInserido.attributes["participacao"] = form.findField('participacao').value;
							ultimoFeatureInserido.attributes["id_estado"] = 1;
							ultimoFeatureInserido.attributes["user_name"] = form.findField('user_name').value;
							ultimoFeatureInserido.attributes["email"] = form.findField('email').value;
							ultimoFeatureInserido.attributes["userid"] = form.findField('userid').value;

							// tem que ser feito o save, para que possamos receber o feature.fid que foi atribuído na base de dados
							saveStrategy.save();
						}
						insertControl.deactivate();
						highlightCtrl.activate();
						// form.reset(); // Não se pode fazer um reset, pois um forma mal preenchido é limpo sem necessidade
					}
				}, /* {
				 text : 'Limpar ',
				 handler : function(button, e) {
				 this.up('form').getForm().reset();
				 }
				 }, */
				{
					text : 'Cancelar',
					handler : function(button, e) {
						console.log('tem que cancelar o ponto inserido...');
						this.up('form').getForm().reset();
						console.log(ultimoFeatureInserido.state);
						ultimoFeatureInserido.state = OpenLayers.State.DELETE;
						console.log(ultimoFeatureInserido.state);
						wfs_ocorrencia.drawFeature(ultimoFeatureInserido);
						insertControl.deactivate();
						highlightCtrl.activate();
						// windowOcorrencia.hide();
						// windowOcorrencia.close();
						this.up('window').close();
					}
				}]
			});

			var formImagemOcorrencia = Ext.create('Ext.form.Panel', {
				// renderTo: 'fi-form',
				// width : 500,
				region : 'south',
				frame : true,
				title : 'Envie uma fotografia',
				// bodyPadding : '10 10 0',
				autoWidth : true,
				autoHeight : true,
				defaults : {
					anchor : '100%',
					allowBlank : false,
					msgTarget : 'side',
					labelWidth : 56
				},
				defaultType : 'textfield',
				items : [{
					xtype : 'hidden',
					name : 'fid'
				}, {
					xtype : 'hidden',
					name : 'user_name',
					value : 'amr'
				}, {
					xtype : 'filefield',
					id : 'form-file',
					emptyText : 'Select an image',
					fieldLabel : 'Photo',
					name : 'photo-path',
					buttonText : '',
					buttonConfig : {
						iconCls : 'upload-icon'
					}
				}],
				buttons : [{
					text : 'Enviar',
					handler : function(button, e) {
						var form = this.up('form').getForm();
						form.findField('fid').setValue(ultimoFeatureInserido.fid);
						// form.findField('user_name').setValue('amr');
						form.findField('user_name').setValue(ultimoFeatureInserido.attributes["user_name"]);
						if (form.isValid()) {
							form.submit({
								url : 'file-upload.php',
								waitMsg : 'A carregar a imagem e a gerar miniaturas...',
								success : function(fp, o) {
									msg('Success', 'A imagem foi guardada no servidor com o nome "' + o.result.caminho);
									// this.up('form').getForm().reset();
									// windowOcorrencia.hide();
									// windowOcorrencia.close();
									button.up('.window').close();
									// ultimoFeatureInserido = null;
								}
							});
						}
					}
				}, {
					text : 'Não enviar fotografia',
					handler : function(button, e) {
						// this.up('form').getForm().reset();
						// windowOcorrencia.hide();
						// windowOcorrencia.close();
						button.up('.window').close();
						// ultimoFeatureInserido = null;
					}
				}]
			});

			// [15:24:06.670] TypeError: a is undefined @ http://amr.osgeopt.pt/extjs-4.1.1/ext-all.js:21

			var popupNewFeature = Ext.create('GeoExt.window.Popup', {
				id : 'windowNewFeature',
				title : 'Adicione...', // feature.attributes["titulo"],
				location : feature,
				width : 300,
				modal : true,
				closeAction : 'destroy',
				// html : feature.attributes["participacao"],
				maximizable : false,
				collapsible : false,
				anchorPosition : 'auto',
				items : new Ext.Panel({
					items : [formOcorrencia, formImagemOcorrencia],
				})

			});
			popupNewFeature.on({
				afterrender : function() {
					console.log('Form vazio para o feature: ' + ultimoFeatureInserido.fid);
				},
				close : function() {
					console.log('destroi popup com os forms');
				},
				beforedestroy : function() {
					console.log('before destroy o popupNewFeature');
				}
			});
			formImagemOcorrencia.disable();
			formOcorrencia.enable();
			console.log('Limpar forms...');
			formOcorrencia.getForm().reset();
			formImagemOcorrencia.getForm().reset();
			if (fb_nome_proprio != '') {
				formOcorrencia.getForm().findField('user_name').setValue(fb_nome_proprio);
				formOcorrencia.getForm().findField('email').setValue(fb_email);
				formOcorrencia.getForm().findField('userid').setValue(fb_id);
			}
			popupNewFeature.show();
		}

		mappanel = Ext.create('GeoExt.panel.Map', {
			title : 'Mapa das ocorrências reportadas',
			map : map,
			region : 'center',
			center : '-26460,101000',
			// EPSG:3763
			zoom : 6, // 6,
			// extent: '12.87,52.35,13.96,52.66',
			// stateful : true,
			stateful : false,
			stateId : 'mappanel',
			tbar : [{
				text : 'Guia passo-a-passo',
				icon : 'images/Map-Marker-Flag-Pink-24x24.png',
				enableToggle : true,
				handler : function() {
					console.log('Mostrar wizard');
					wizard.show();
				}
			}, '->', {
				xtype : "gx_geocodercombo",
				layer : locationLayer,
				minChars : 5,
				emptyText : "Pesquisa",
				queryDelay : 500,
				// To restrict the search to a bounding box, uncomment the following
				// line and change the viewboxlbrt parameter to a left,bottom,right,top
				// bounds in EPSG:4326:
				url : "http://nominatim.openstreetmap.org/search?format=json&viewboxlbrt=-9,39,-8,41",
				width : 200
			}] /*,
			 dockedItems : [{
			 xtype : 'toolbar',
			 dock : 'top',
			 items : [{
			 text : 'Current center of the map',
			 handler : function() {
			 var c = GeoExt.panel.Map.guess().map.getCenter();
			 var z = GeoExt.panel.Map.guess().map.getZoom();
			 console.debug(z);
			 Ext.Msg.alert(this.getText(), c.toString() + ' zoom=' + z);
			 }
			 }, {
			 xtype : 'tbfill'
			 }, {
			 text : 'Ocorrências por estado',
			 handler : function() {
			 wfs_ocorrencia.styleMap.styles["default"] = style_estado;
			 wfs_ocorrencia.redraw();
			 }
			 }, {
			 text : 'Contribuir',
			 icon : 'images/Map-Marker-Flag-Pink-24x24.png',
			 enableToggle: true,
			 handler : function() {
			 wfs_ocorrencia.styleMap.styles["default"] = style;
			 wfs_ocorrencia.redraw();
			 }
			 }]
			 }] */
		});

		Ext.define('Contribuicoes.Model', {
			extend : 'Ext.data.Model',
			fields : [{
				name : 'id_ocorrencia',
				type : 'int'
			}, {
				name : 'data_hora',
				type : 'date',
				dateFormat : 'c'
			}, {
				name : 'id_estado',
				type : 'int'
			}, {
				name : 'id_tipo_ocorrencia',
				type : 'int'
			}, {
				name : 'titulo',
				type : 'string'
			}, {
				name : 'participacao',
				type : 'string'
			}, {
				name : 'user_name',
				type : 'string'
			}, {
				name : 'apagado',
				type : 'boolean'
			}, {
				name : 'estado',
				type : 'string'
			}, {
				name : 'the_geom',
				type : 'string'
			}]
		});

		var storeContribuicoesJson = new Ext.data.JsonStore({
			model : 'Contribuicoes.Model',
			proxy : {
				type : 'ajax',
				url : 'getOcorrenciasJson.php',
				reader : {
					type : 'json',
					root : 'ocorrencias',
					totalProperty : 'total'
				}
			},
			autoLoad : true
		});

		var gridPanelContribuicoesNovo = Ext.create('Ext.grid.GridPanel', {
			header : false,
			// title : "Participações registadas",
			// region: "east",
			store : storeContribuicoesJson,
			width : 420,
			columns : [{
				header : "Título",
				width : 100,
				dataIndex : "titulo"
			}, {
				header : "Participação",
				width : 100,
				dataIndex : "participacao"
			}, {
				header : "Reportado por",
				width : 80,
				dataIndex : "user_name"
			}, {
				header : "Em",
				width : 80,
				xtype : 'datecolumn',
				dateFormat : 'c', // impecável: a data vem no formato ISO8601
				dataIndex : "data_hora"
			}, {
				header : "Estado",
				width : 80,
				dataIndex : "estado"
			}],
			selType : 'featuremodel',
			listeners : {
				itemclick : function(grid, record, item, index, event) {
					// itemdblclick : function(grid, record, item, index, event) {
					console.debug(record);
					console.debug(record.data["the_geom"]);
					var geometry = OpenLayers.Geometry.fromWKT(record.data["the_geom"]);
					console.debug(geometry);
					// console.log(record.data["participacao"]);
					// geometry = record.raw["geometry"];
					// console.log(geometry);
					centro = geometry.getBounds().getCenterLonLat();
					console.log(centro);
					map.setCenter(centro, 8);
				}
			}
		});

		var w = Ext.create('Ext.Window', {
			title : 'Participações registadas',
			width : 480,
			height : 200,
			closable : false,
			x : 10,
			y : 450,
			plain : true,
			// headerPosition : 'bottom',
			layout : 'fit',
			items : [gridPanelContribuicoesNovo]
		});
		w.show();
		w.alignTo(Ext.getBody(), "tr-tr", [-10, 10]);

		var myData = [['<img src="images/Mf_node.png" alt="Pontos"/>', 'Pontos'], ['<img src="images/Mf_way.png" alt="Linhas"/>', 'Linhas'], ['<img src="images/Mf_area.png" alt="Áreas"/>', 'Áreas'], ['<img src="images/Mf_node_area.png" alt="Pontos e Áreas"/>', 'Pontos e Áreas'], ['<img src="images/Mf_node_way.png" alt="Pontos e Linhas"/>', 'Pontos e Linhas'], ['<img src="images/Mf_node_way_area.png" alt="Pontos, Linhas e Áreas"/>', 'Pontos, Linhas e Áreas'], ['<img src="images/Mf_node_way_area_PB.png" alt="Pontos, Linhas e Áreas"/>', 'Pontos, Linhas e Áreas (na mesma cor)'], ['<img src="images/Mf_raster.png" alt="Superfície"/>', 'Superfície (<i>raster</i>)']];
		var storeIcons = new Ext.data.SimpleStore({
			fields : ['icone', 'significado']
		});
		storeIcons.loadData(myData);
		var gridLegenda = new Ext.grid.GridPanel({
			store : storeIcons,
			// width: 280,
			// stripeRows: true,
			// autoExpandColumn: 'significado',
			columns : [{
				header : "Símbolo",
				width : 68,
				dataIndex : 'icone'
			}, {
				id : 'significado',
				header : "Significado",
				width : 215,
				dataIndex : 'significado'
			}]
		});

		var storeTree = Ext.create('Ext.data.TreeStore', {
			model : 'GeoExt.data.LayerTreeModel',
			root : {
				expanded : true,
				children : [{
					plugins : ['gx_baselayercontainer'],
					expanded : true,
					text : "Mapas de base"
				}, {
					plugins : ['gx_overlaylayercontainer'],
					expanded : true,
					text : "Camadas disponíveis"
				}]
			}
		});

		treeCamadas = Ext.create('GeoExt.tree.Panel', {
			border : true,
			// region: "west",
			title : "Camadas",
			width : 200,
			split : true,
			collapsible : true,
			// collapseMode : "mini",
			autoScroll : true,
			store : storeTree,
			rootVisible : false,
			lines : false,
		});

		// http://geoext.github.io/geoext2/examples/legendpanel/legendpanel.html
		legendPanel = Ext.create('GeoExt.panel.Legend', {
			defaults : {
				labelCls : 'mylabel',
				style : 'padding:5px'
			},
			bodyStyle : 'padding:5px',
			width : 350,
			autoScroll : true,
			title : "Legenda"
			// region : 'west'
		});

		Ext.create('Ext.container.Viewport', {
			layout : 'border',
			items : [{
				region : 'north',
				xtype : 'panel',
				height : 100,
				collapsible : false,
				border : true,
				// bodyCssClass : 'painelNorte',
				contentEl : 'norte'
			}, {
				region : 'east',
				id : 'east-panel',
				title : 'Informações',
				//border: true,
				autoScroll : true,
				width : 300,
				split : true,
				collapsedTitle : 'Clique aqui para ver as Informações',
				collapsible : true,
				collapsed : true,
				//collapseMode: 'mini',
				//collapsesize: 5,
				minSize : 200,
				cmargins : '0 10 0 0',
				//headerAsText :true,
				//header:true,
				layout : 'accordion',
				layoutConfig : {
					animate : true
				},
				frame : true,
				items : [treeCamadas, legendPanel, /* gridPanelContribuicoesNovo, gridVias, */
				{
					title : 'Sobre A Minha Rua',
					id : 'bemvindo',
					autoScroll : true,
					// collapsed: false,
					contentEl : 'bemvindotext'
				}, {
					title : 'Participe!',
					id : 'regulamento',
					autoScroll : true,
					contentEl : 'legendapdm'
				}, /*{
				 title : 'Legenda',
				 id : 'legenda',
				 autoScroll : true,
				 items : [{
				 contentEl : 'legendaplantas'
				 }, gridLegenda]
				 }, */
				{
					title : 'Créditos',
					id : 'info',
					contentEl : 'infotext'
				}]
			}, mappanel],
			monitorResize : true,
			listeners : {
				resize : {
					fn : function(el) {
						w.alignTo(Ext.getBody(), "br-br", [-10, -80]);
					}
				}
			}
		});

		var cards = new Array();

		// first card with welcome message
		cards.push(Ext.create('Ext.ux.wizard.Card', {
			title : 'Bem vindo à "A Minha Rua"!',
			showTitle : true,
			titleCls : '',
			titleStyle : 'font-size: 2.5em;',
			baseCls : 'rnd1',
			items : [{
				border : false,
				bodyStyle : 'background:none;',
				html : 'Bem vindo à nova plataforma de participação <strong>A Minha Rua</strong>, ' + 'onde poderá reportar, com o conhecimento que tem <i>da sua rua</i>, os problemas que encontra, para que estes sejam rapidamente solucionados.' + '<br/><br/>' + 'Por favor clique no botão <strong><i>Seguinte</strong></i> para prosseguir no guia passo a passo.'
			}, {
				xtype : 'image',
				src : 'images/plpa.jpg',
				height : 149,
				width : 168,
				border : true,
				// style : 'padding-left:60px',
				resizable : false
			}]
		}));

		// second card with input fields last/firstname
		cards.push(new Ext.ux.wizard.Card({
			title : 'Navegue no mapa',
			showTitle : true,
			titleCls : '',
			titleStyle : 'font-size: 2.5em;',
			monitorValid : true,
			baseCls : 'rnd1',
			defaults : {
				labelStyle : 'font-size:12px'
			},
			fieldDefaults : {
				labelAlign : 'right',
				msgTarget : 'none',
				invalidCls : '' //unset the invalidCls so individual fields do not get styled as invalid
			},
			items : [{
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'Para se mover no mapa, arraste-o de um lado para o outro (com o botão esquedro do rato pressionado)'
			}, {
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'Amplie e reduza o mapa utilizando a "roda" do rato.<br/>' + 'Em alternativa, pode também usar os botões <strong>+</strong> e <strong>-</strong> que aparecem no canto superior esquerdo do mapa'
			}, {
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'No lado direito, o painel "Informações" encontra-se parcialmente oculto. ' + 'Abra-o e dentro do separador <i>Camadas</i> pode ver e esconder as camadas de informação que disponíveis para visualização no mapa'
			}, {
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'Procure uma rua ou local recorrendo à caixa de <strong>pequisa</strong> na barra superior do mapa'
			}]
		}));

		// third card with input field email-address
		cards.push(new Ext.ux.wizard.Card({
			title : 'Veja outras participações',
			showTitle : true,
			titleCls : '',
			titleStyle : 'font-size: 2.5em;',
			monitorValid : true,
			baseCls : 'rnd1',
			// margins : '10 10 10 10',
			defaults : {
				labelStyle : 'font-size:11px'
			},
			items : [{
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'As participações aparecem no mapa assinaladas com um símbolo'
			}, {
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'A janela com todas as participações está sempre presente'
			}, {
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'Selecione uma participação nessa janela, para centrar o mapa nesse local'
			}, {
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'Clique com o botão esquerdo do rato sobre uma contribuição existente para ver os detalhes da mesma'
			}]
		}));

		cards.push(new Ext.ux.wizard.Card({
			title : 'Participe!',
			showTitle : true,
			titleCls : '',
			titleStyle : 'font-size: 2.5em;',
			monitorValid : true,
			baseCls : 'rnd1',
			defaults : {
				labelStyle : 'font-size:12px'
			},
			fieldDefaults : {
				labelAlign : 'right',
				msgTarget : 'none',
				invalidCls : '' //unset the invalidCls so individual fields do not get styled as invalid
			},
			items : [{
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'A sua participação é muito bem vinda e indispensável para a melhoria da sua rua na sua cidade.'
			}, {
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'Pode participar de duas formas:'
			}, {
				border : false,
				bodyStyle : 'background:none;padding-bottom:5px;padding-left:30px;',
				html : 'Submetendo uma nova participação'
			}, {
				border : false,
				bodyStyle : 'background:none;padding-bottom:5px;padding-left:30px;',
				html : 'Comentando uma participação já registada'
			}]
		}));

		cards.push(new Ext.ux.wizard.Card({
			title : 'Nova participação',
			showTitle : true,
			titleCls : '',
			titleStyle : 'font-size: 2.5em;',
			monitorValid : true,
			baseCls : 'rnd1',
			defaults : {
				labelStyle : 'font-size:12px'
			},
			fieldDefaults : {
				labelAlign : 'right',
				msgTarget : 'none',
				invalidCls : '' //unset the invalidCls so individual fields do not get styled as invalid
			},
			items : [{
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'Comece por centrar o mapa no local onde pretende fazer a sua participação'
			}, {
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'Carregue no botão "Quero contribuir!" e clique no local pretendido'
			}, {
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'Automaticamente aparece-lhe o formulário para descrever a sua participação'
			}, {
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'Se clicou no local errado, pode cancelar esta participação para depois recomeçar'
			}]
		}));

		cards.push(new Ext.ux.wizard.Card({
			title : 'Descreva a sua participação',
			showTitle : true,
			titleCls : '',
			titleStyle : 'font-size: 2.5em;',
			monitorValid : true,
			baseCls : 'rnd1',
			defaults : {
				labelStyle : 'font-size:12px'
			},
			fieldDefaults : {
				labelAlign : 'right',
				msgTarget : 'none',
				invalidCls : '' //unset the invalidCls so individual fields do not get styled as invalid
			},
			items : [{
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'Comece por definir o assunto da sua participação, em 3 ou 4 palavras no máximo'
			}, {
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'Detalhe a sua participação, com o texto que for necessário para descrever o problema'
			}, {
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'Preencha o seu nome, preferencialmente utilizando o primeiro e último nome'
			}, {
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'Indique o seu endereço de email, para receber notificações sobre a sua participação' + '<ul>' + '<li>Receberá por email a notificação de que a mesma foi registada no sistema' + '<li>Sempre que a sua participação for comentada, será notificado por email' + '</ul>'
			}, {
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'Carregue em <strong>Contribuir</strong> para avançar'
			}]
		}));

		cards.push(new Ext.ux.wizard.Card({
			title : 'Ilustre a sua participação',
			showTitle : true,
			titleCls : '',
			titleStyle : 'font-size: 2.5em;',
			monitorValid : true,
			baseCls : 'rnd1',
			defaults : {
				labelStyle : 'font-size:12px'
			},
			fieldDefaults : {
				labelAlign : 'right',
				msgTarget : 'none',
				invalidCls : '' //unset the invalidCls so individual fields do not get styled as invalid
			},
			items : [{
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'Se puder, antecipadamente tire uma fotografia ao local ou contexto da sua participação'
			}, {
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'Se tem uma fotografia, carregue no botão para escolher a fotografia previamente guardada no computador'
			}, {
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'Escolha <strong>Enviar</strong> para terminar a sua participação'
			}, {
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'Se não tem uma fotografia a ilustrar a sua participação, salte este passo e escolha <strong>Não enviar fotografia</strong> para terminar.'
			}]
		}));

		cards.push(new Ext.ux.wizard.Card({
			title : 'Comentar contribuições existentes',
			showTitle : true,
			titleCls : '',
			titleStyle : 'font-size: 2.5em;',
			monitorValid : true,
			baseCls : 'rnd1',
			defaults : {
				labelStyle : 'font-size:12px'
			},
			fieldDefaults : {
				labelAlign : 'right',
				msgTarget : 'none',
				invalidCls : '' //unset the invalidCls so individual fields do not get styled as invalid
			},
			items : [{
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'Pode acrescentar um comentário construtivo a qualquer participação anteriormente submetida. O seu comentárioé importante para validar e reforçar as participações'
			}, {
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'Abra o dealhe dpa participação que pretende comentar, clicando com o botão esquerdo do rato sobre a mesma'
			}, {
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'Preencha o seu comentário e o seu nome (primeiro e último nome)'
			}, {
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'Escolha <strong>Comentar</strong> para terminar'
			}]
		}));

		// fourth card with finish-message
		cards.push(new Ext.ux.wizard.Card({
			title : 'Fim!',
			showTitle : true,
			titleCls : '',
			titleStyle : 'font-size: 2.5em;',
			monitorValid : true,
			baseCls : 'rnd1',
			items : [{
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'Pronto para participar? '
			}, {
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'Ao carregar em "Terminar" este guia será concluído'
			}, {
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'Sempre que quiser, pode voltar a ver este guia passo a passo, carregando no botão <strong>Guia passo a passo</strong>'
			}, {
				border : false,
				bodyStyle : 'background:none;padding-bottom:10px;',
				html : 'A sua rua conta consigo!'
			}]
		}));

		var wizard = new Ext.ux.Wizard({

			// title: 'A simple example for a wizard'

			// no headConfig suplied no header will be shown.
			headConfig : {
				// title: 'Simple Wizard Head title Example',
				headerPosition : 'top',
				position : 'top'// or bottom
				,
				stepText : "<center>Step {0} of {1}: {2}</center>"
			},

			// no sideConfig suplied no header will be shown.
			sideConfig : {
				// title: 'Simple Wizard Side title Example',
				headerPosition : 'left',
				position : 'left' // or right
			},

			width : 450, // 850,
			height : 400, // 800,
			closable : false,
			closeAction : 'hide',

			headConfig : false, // true,
			sideConfig : false, // true,

			cardPanelConfig : {
				defaults : {
					baseCls : 'x-small-editor',
					bodyStyle : 'padding:40px 15px 5px 120px;background-color:#F6F6F6;',
					border : false
				},
				layout : 'card'
			},

			cards : cards
		});

		/*
		 wizard.on('finish', function(me, data) {
		 alert("done. Thanks for enterring the following data\n" + Ext.encode(data));
		 });

		 wizard.on('cancel', function(me, data) {
		 alert("Sorry you cancelled.\nYou enterred\n" + Ext.encode(data));
		 });
		 */

	}
});
