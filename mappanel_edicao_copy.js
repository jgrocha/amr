/*
 * Copyright (c) 2008-2012 The Open Source Geospatial Foundation
 *
 * Published under the BSD license.
 * See https://github.com/geoext/geoext2/blob/master/license.txt for the full
 * text of the license.
 */

Ext.require(['Ext.container.Viewport', 'Ext.state.Manager', 'Ext.TaskManager', 'Ext.form.field.File', 'Ext.form.field.ComboBox', 'Ext.form.Panel', 'Ext.state.CookieProvider', 'Ext.window.MessageBox', 'GeoExt.panel.Map', 'GeoExt.window.Popup', 'Ext.form.field.Hidden']);
Ext.require(['Ext.grid.*', 'Ext.data.*', 'Ext.util.*', 'Ext.grid.PagingScroller', 'Ext.ux.form.SearchField']);
Ext.require(['Ext.layout.container.Border', 'Ext.tab.Panel']);

Ext.application({
	name : 'AMR-Alfa',
	launch : function() {

		Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider', {
			expires : new Date(new Date().getTime() + (1000 * 60 * 60 * 24 * 7))
			// 7 days from now
		}));

		OpenLayers.DOTS_PER_INCH = 25.4 / 0.28;
		WmsImageformat = 'image/png';

		// OpenLayers Proxy
		OpenLayers.ProxyHost = "/OpenLayers-2.12/examples/proxy.cgi?url=";

		var bounds = new OpenLayers.Bounds(-119191.40749999962, -300404.80399999936, 162129.08110000013, 276083.7674000006);
		var options = {
			maxExtent : bounds,
			// scales : [1000000, 500000, 250000, 100000, 50000, 25000,
			// 10000, 5000, 1000, 500],
			resolutions : [279.9998488001, 139.9999244, 69.9999622, 27.99998488, 13.99999244, 6.99999622, 2.799998488, 1.399999244, 0.2799998488, 0.1399999244],
			// resolutions : [140.0, 70.0, 27.999999999999996,
			// 13.999999999999998, 6.999999999999999, 2.8, 1.4,
			// 0.27999999999999997, 0.13999999999999999],
			projection : "EPSG:3763",
			displayProjection : new OpenLayers.Projection("EPSG:3763"),
			title : 'Mapa',
			units : 'm',
			allOverlays : true
		};
		map = new OpenLayers.Map(options);

		map.addControl(new OpenLayers.Control.ScaleLine());
		map.addControl(new OpenLayers.Control.Scale());
		map.addControl(new OpenLayers.Control.Attribution());
		var posicaoRato = new OpenLayers.Control.MousePosition({
			id : "coordenadas_rato"
		});
		map.addControl(posicaoRato);
		// map.addControl(new OpenLayers.Control.PanZoomBar());
		map.addControl(new OpenLayers.Control.LayerSwitcher());
		map.addControl(new OpenLayers.Control.Permalink('permalink'));

		var wms_municipios = new OpenLayers.Layer.WMS("Municípios", "http://webgis.di.uminho.pt/geoserver/gwc/service/wms?", {
			layers : 'amr:concelhos',
			format : WmsImageformat,
			transparent : true
		}, {
			singleTile : false,
			ratio : 1,
			isBaseLayer : true,
			visibility : true,
			iconCls : "xnd-icon",
			icon : 'images/Mf_area.png',
			qtip : 'Municípios'
		});

		var wms_freguesias = new OpenLayers.Layer.WMS("Freguesias", "http://webgis.di.uminho.pt/geoserver/gwc/service/wms?", {
			layers : 'amr:freguesias',
			format : WmsImageformat,
			transparent : true
		}, {
			singleTile : false,
			ratio : 1,
			isBaseLayer : true,
			visibility : true,
			minScale : 100000,
			iconCls : "xnd-icon",
			icon : 'images/Mf_area.png',
			qtip : 'Freguesias'
		});

		var osm = new OpenLayers.Layer.WMS("OpenStreetMap", "http://webgis.di.uminho.pt/geoserver/gwc/service/wms?", {
			layers : 'osmpt3763',
			format : WmsImageformat,
			transparent : true
		}, {
			singleTile : false,
			ratio : 1,
			isBaseLayer : true,
			visibility : true,
			iconCls : "xnd-icon",
			icon : 'images/Mf_area.png',
			qtip : 'OpenStreetMap'
		});

		var context = {
			getColor : function(feature) {
				var cor;
				// console.debug(feature);
				switch (parseInt(feature.attributes["id_tipo_ocorrencia"])) {
					case 1:
						cor = 'Red';
						break;
					// #FF0000
					case 2:
						cor = 'Olive';
						break;
					// #808000
					case 3:
						cor = 'OliveDrab';
						break;
					// #6B8E23
					case 4:
						cor = 'Orange';
						break;
					// #FFA500
					case 5:
						cor = 'OrangeRed';
						break;
					// #FF4500
					case 6:
						cor = 'Orchid';
						break;
					// #DA70D6
					case 7:
						cor = 'PaleGoldenRod';
						break;
					// #EEE8AA
					case 8:
						cor = 'PaleGreen';
						break;
					// #98FB98
					case 9:
						cor = 'PaleTurquoise';
						break;
					// #AFEEEE
					case 10:
						cor = 'PaleVioletRed';
						break;
					// #DB7093
					case 11:
						cor = 'PapayaWhip';
						break;
					// #FFEFD5
					case 12:
						cor = 'PeachPuff';
						break;
					// #FFDAB9
					case 13:
						cor = 'Peru';
						break;
					// #CD853F
					case 14:
						cor = 'Pink';
						break;
					// #FFC0CB
					case 15:
						cor = 'Plum';
						break;
					// #DDA0DD
					case 16:
						cor = 'PowderBlue';
						break;
					// #B0E0E6
					case 17:
						cor = 'Purple';
						break;
					// #800080
					default:
						cor = 'gray';
				}
				return cor;
			}
		};
		var template = {
			cursor : "pointer",
			fillOpacity : 0.5,
			fillColor : "${getColor}",
			pointRadius : 5,
			strokeWidth : 1,
			strokeOpacity : 1,
			strokeColor : "${getColor}",
			graphicName : "triangle"
		};
		var style = new OpenLayers.Style(template, {
			context : context
		});

		var context_2 = {
			getColor : function(feature) {
				var cor_2;
				// console.debug(feature);
				switch (parseInt(feature.attributes["id_estado"])) {
					case 1:
						cor_2 = 'Red';
						break;
					// #FF0000
					case 2:
						cor_2 = 'PaleGreen';
						break;
					// #98FB98
					default:
						cor_2 = 'gray';
				}
				return cor_2;
			}
		};
		var template_estado = {
			cursor : "pointer",
			fillOpacity : 0.5,
			fillColor : "${getColor}",
			pointRadius : 5,
			strokeWidth : 1,
			strokeOpacity : 1,
			strokeColor : "${getColor}",
			graphicName : "square"
		};
		var style_estado = new OpenLayers.Style(template_estado, {
			context : context_2
		});

		var saveStrategy = new OpenLayers.Strategy.Save();
		saveStrategy.events.register('success', null, saveSuccess);
		saveStrategy.events.register('fail', null, saveFail);

		var wfs_ocorrencia = new OpenLayers.Layer.Vector('Participações', {
			strategies : [new OpenLayers.Strategy.BBOX(), saveStrategy],
			styleMap : new OpenLayers.StyleMap({
				'default' : style_estado
			}),
			protocol : new OpenLayers.Protocol.WFS({
				url : 'http://webgis.di.uminho.pt/geoserver/wfs',
				featureType : 'ocorrencias',
				featureNS : 'http://postgis.org',
				srsName : 'EPSG:3763',
				version : '1.1.0',
				reportError : true,
				featurePrefix : 'amr',
				schema : 'http://webgis.di.uminho.pt/geoserver/wfs/DescribeFeatureType?version=1.1.0&typename=amr:ocorrencias',
				geometryName : 'the_geom'
			}),
			visibility : true
		});

		map.addLayers([wms_municipios, wms_freguesias, osm, wfs_ocorrencia]);

		var mostraDados = function(e) {
			createPopup(e.feature);
		};
		var limpaDados = function(e) {
			// removePopup();
			var comWin = Ext.getCmp('pipocas');
			comWin.close();
		};

		var highlightCtrl = new OpenLayers.Control.SelectFeature(wfs_ocorrencia, {
			hover : true,
			highlightOnly : true,
			eventListeners : {
				beforefeaturehighlighted : mostraDados,
				featureunhighlighted : limpaDados
			}
		});

		var formComentario = Ext.create('Ext.form.Panel', {
			// renderTo: 'fi-form',
			// width : 500,
			region : 'center',
			frame : true,
			title : 'Comentar',
			// bodyPadding : '10 10 0',
			autoWidth : true,
			autoHeight : true,
			defaults : {
				anchor : '100%',
				allowBlank : false,
				msgTarget : 'side',
				labelWidth : 0
			},
			items : [{
				xtype : 'textareafield',
				grow : true,
				name : 'comentario',
				allowBlank : false
			}],
			buttons : [{
				text : 'Comentar',
				handler : function() {
					var form = this.up('form').getForm();
					if (form.isValid()) {
						console.log('Vai fazer submit do comentário...');
					}
				}
			}, {
				text : 'Limpar ',
				handler : function() {
					this.up('form').getForm().reset();
				}
			}]
		});

		function createPopup(feature) {
			// console.debug(feature);
			// todos os features sacados com o WFS têm um feature.attributes["id_ocorrencia"]
			// todos os features sacados com o WFS têm um feature.fid
			// no entanto, os novos, criados com o OpenLayers, não têm feature.attributes["id_ocorrencia"]
			// têm APENAS feature.fid, que é atribuído quando são gravados
			popup = Ext.create('GeoExt.window.Popup', {
				id : 'pipocas',
				title : feature.attributes["titulo"],
				location : feature,
				width : 400,
				// modal : true,
				closeAction: 'close',
				unpinnable : false,
				maximizable : false,
				collapsible : false,
				anchorPosition : 'auto',
				items : [{
					// title : 'Bogus Tab',
					html : feature.attributes["participacao"],
					loader : {
						url : 'getOcorrencia.php',
						scripts : true,
						autoLoad : true,
						params : {
							fid : feature.fid, // feature.attributes["id_ocorrencia"],
							user_name : 'amr'
						},
					},
				}, formComentario],
				/*
				 loader : {
				 url : 'getOcorrencia.php',
				 scripts : true,
				 autoLoad : true,
				 params : {
				 fid : feature.fid, // feature.attributes["id_ocorrencia"],
				 user_name : 'amr'
				 },
				 },
				 */
				dockedItems : [{
					dock : 'bottom',
					xtype : 'toolbar',
					items : [{
						xtype : 'button',
						text : '+1'
					}, {
						xtype : 'button',
						text : 'Denunciar'
					}, {
						xtype : 'button',
						text : 'Sugerir localização'
					}, {
						xtype : 'button',
						text : 'X',
						handler : function() {
							this.up('window').hide();
						}
					}]
				}]
			});
			// unselect feature when the popup
			// is closed
			popup.on({
				close : function() {
					console.log(OpenLayers.Util.indexOf(wfs_ocorrencia.selectedFeatures, this.feature));
					if (OpenLayers.Util.indexOf(wfs_ocorrencia.selectedFeatures, this.feature) > -1) {
						// selectCtrl.unselect(this.feature);
						highlightCtrl.unselect(this.feature);
					}
				}
			});
			// console.log('Vai carregar a ocorrência ' + feature.attributes["id_ocorrencia"]);
			// popup.getLoader().load();
			popup.show();
		}

		// map.addControl(highlightCtrl);
		// highlightCtrl.activate();

		// var navControl = new OpenLayers.Control.Navigation();
		var modifyControl = new OpenLayers.Control.ModifyFeature(wfs_ocorrencia);
		var insertControl = new OpenLayers.Control.DrawFeature(wfs_ocorrencia, OpenLayers.Handler.Point, {
			'displayClass' : 'olControlDrawFeaturePoint'
		});

		// var panelControls = [navControl, highlightCtrl, modifyControl, insertControl];
		var panelControls = [highlightCtrl, modifyControl, insertControl];
		var toolbar = new OpenLayers.Control.Panel({
			displayClass : 'olControlEditingToolbar',
			defaultControl : panelControls[0]
		});
		toolbar.addControls(panelControls);
		map.addControl(toolbar);

		var ultimoFeatureInserido = null;

		insertControl.events.on({
			featureadded : function(event) {
				// featureadded : function(event) {
				ultimoFeatureInserido = event.feature;
				// var geometry = f.geometry;formOcorrencia
				// console.debug(ultimoFeatureInserido);
				// ultimoFeatureInserido.attributes["titulo"] = 'Acabadinho de Inserir';
				// console.log('Vou ler os atributos numa janela para o feature.id = ' + ultimoFeatureInserido.id + ' com o estado ' + ultimoFeatureInserido.state);

				windowOcorrencia.show();
				windowOcorrencia.alignTo(Ext.getBody(), "bl-bl", [10, -10]);

				/*
				 console.log('Ui... só devia aparecer depois de o form ser fechado... ou não?');
				 console.debug(formOcorrencia);
				 console.debug(formOcorrencia.getForm());
				 console.debug(formOcorrencia.getForm().findField('id_tipo_ocorrencia').value);
				 */

				formImagemOcorrencia.disable();
				formOcorrencia.enable();
				// formOcorrencia.getForm().reset();
				// formImagemOcorrencia.getForm().reset();
			}
		});
		Ext.define('TipoOcorrencia', {
			extend : 'Ext.data.Model',
			fields : [{
				name : 'id',
				type : 'int'
			}, {
				name : 'ocorrencia',
				type : 'string'
			}]
		});
		var tipo_ocorrencia_store = [{
			"id" : 1,
			"ocorrencia" : "Acessos para Cidadãos com Mobilidade Reduzida"
		}, {
			"id" : 2,
			"ocorrencia" : "Animais Abandonados"
		}, {
			"id" : 3,
			"ocorrencia" : "Conservação da Iluminação Pública"
		}, {
			"id" : 4,
			"ocorrencia" : "Conservação das Ruas e Pavimento"
		}, {
			"id" : 5,
			"ocorrencia" : "Conservação de Parque Escolar"
		}, {
			"id" : 6,
			"ocorrencia" : "Estacionamento de Veículos"
		}, {
			"id" : 7,
			"ocorrencia" : "Limpeza de Valetas, Bermas e Caminhos"
		}, {
			"id" : 8,
			"ocorrencia" : "Limpeza e Conservação de Espaços Públicos"
		}, {
			"id" : 9,
			"ocorrencia" : "Manutenção de Ciclovias"
		}, {
			"id" : 10,
			"ocorrencia" : "Manutenção e Limpeza de Contentores e Ecopontos"
		}, {
			"id" : 11,
			"ocorrencia" : "Manutenção Rega e Limpeza de Jardins"
		}, {
			"id" : 12,
			"ocorrencia" : "Poluição Sonora"
		}, {
			"id" : 13,
			"ocorrencia" : "Publicidade, Outdoors e Cartazes"
		}, {
			"id" : 14,
			"ocorrencia" : "Recolha de Lixo"
		}, {
			"id" : 15,
			"ocorrencia" : "Rupturas de Águas ou Desvio de Tampas"
		}, {
			"id" : 16,
			"ocorrencia" : "Sinalização de Trânsito"
		}, {
			"id" : 17,
			"ocorrencia" : "Acessos para Cidadãos com Mobilidade Reduzida"
		}];

		var store = Ext.create('Ext.data.Store', {
			model : 'TipoOcorrencia',
			data : tipo_ocorrencia_store
		});

		// http://dev.sencha.com/deploy/ext-4.1.0-gpl/examples/form/file-upload.php

		// inspiração:
		// http://www.fixmystreet.com/around?pc=Tib+st%2C+manchester#report
		// var fsf = new Ext.FormPanel({

		/*
		 var advertisementRefresherTask = {
		 run : updateClock,
		 interval : 5000,
		 repeat : 5
		 }

		 Ext.TaskMgr.start(advertisementRefresherTask);

		 function doAjax() {
		 Ext.Ajax.request({
		 url : '${createLinkTo(dir:'advertisement/getNextAdvertisement')}' ,
		 method: 'GET',
		 success: function ( result, request) {
		 var jsonData = Ext.util.JSON.decode(result.responseText);
		 var advertisementDiv = document.getElementById('advertisement');
		 advertisementDiv.innerHTML = jsonData.html;
		 },
		 failure: function ( result, request) {
		 Ext.MessageBox.alert('Failed', 'Failed');
		 }
		 });
		 }

		 var updateClock = function() {
		 console.log('Evento ciclico ' + new Date().format('g:i:s A'));
		 }

		 var task = Ext.TaskManager.start({
		 run : updateClock,
		 interval : 2000
		 });
		 */
		var msg = function(title, msg) {
			Ext.Msg.show({
				title : title,
				msg : msg,
				minWidth : 200,
				modal : true,
				icmodal : true,
				on : Ext.Msg.INFO,
				buttons : Ext.Msg.OK
			});
		};

		function saveSuccess(event) {
			// só agora tenho o fid atribuído... fixe, que é para ser atribuído à imagem.
			console.log('Your mapped field(s) have been successfully saved, em particular ' + ultimoFeatureInserido.fid);
			console.debug(event.response);
		}

		function saveFail(event) {
			console.log('Error! Your changes could not be saved. ');
			console.debug(event.response);
			// alert('Error! Your changes could not be saved. ');
		}

		var required = '<span style="color:red;font-weight:bold" data-qtip="Obrigatório">*</span>';
		var formOcorrencia = Ext.create('Ext.form.Panel', {
			// renderTo: 'fi-form',
			// width : 500,
			region : 'center',
			frame : true,
			title : 'Descreva a ocorrência',
			// bodyPadding : '10 10 0',
			autoWidth : true,
			autoHeight : true,
			defaults : {
				anchor : '100%',
				allowBlank : false,
				msgTarget : 'side',
				labelWidth : 50
			},
			defaultType : 'textfield',
			items : [{
				fieldLabel : 'Título',
				name : 'titulo',
				allowBlank : false,
				afterLabelTextTpl : required
			}, {
				fieldLabel : 'Tipo',
				name : 'id_tipo_ocorrencia',
				xtype : 'combo',
				queryMode : 'local',
				// width : 280,
				emptyText : 'Escolha o tipo de ocorrência',
				store : store,
				displayField : 'ocorrencia',
				valueField : 'id',
				afterLabelTextTpl : required
			}, {
				xtype : 'textareafield',
				grow : true,
				name : 'participacao',
				fieldLabel : 'Descrição',
				anchor : '100%',
				afterLabelTextTpl : required
			}],

			buttons : [{
				text : 'Registar',
				handler : function() {
					var form = this.up('form').getForm();
					if (form.isValid()) {
						formImagemOcorrencia.enable();
						formOcorrencia.disable();
						console.debug(ultimoFeatureInserido);
						ultimoFeatureInserido.attributes["titulo"] = this.up('form').getForm().findField('titulo').value;
						ultimoFeatureInserido.attributes["id_tipo_ocorrencia"] = this.up('form').getForm().findField('id_tipo_ocorrencia').value;
						ultimoFeatureInserido.attributes["participacao"] = this.up('form').getForm().findField('participacao').value;
						ultimoFeatureInserido.attributes["id_estado"] = 1;
						ultimoFeatureInserido.attributes["user_name"] = 'amr';
						// tem que ser feito o save, para que possamos receber o feature.fid que foi atribuído na base de dados
						saveStrategy.save();
					}
					insertControl.deactivate();
					highlightCtrl.activate();
				}
			}, {
				text : 'Limpar ',
				handler : function() {
					this.up('form').getForm().reset();
				}
			}, {
				text : 'Cancelar',
				handler : function() {
					console.log('tem que cancelar o ponto inserido...');
					this.up('form').getForm().reset();
					console.log(ultimoFeatureInserido.state);
					ultimoFeatureInserido.state = OpenLayers.State.DELETE;
					console.log(ultimoFeatureInserido.state);
					wfs_ocorrencia.drawFeature(ultimoFeatureInserido);
					insertControl.deactivate();
					highlightCtrl.activate();
					windowOcorrencia.hide();
				}
			}]
		});

		var formImagemOcorrencia = Ext.create('Ext.form.Panel', {
			// renderTo: 'fi-form',
			// width : 500,
			region : 'south',
			frame : true,
			title : 'Envie uma fotografia',
			// bodyPadding : '10 10 0',
			autoWidth : true,
			autoHeight : true,
			defaults : {
				anchor : '100%',
				allowBlank : false,
				msgTarget : 'side',
				labelWidth : 56
			},
			defaultType : 'textfield',
			items : [{
				xtype : 'hidden',
				name : 'fid'
			}, {
				xtype : 'hidden',
				name : 'user_name',
				value : 'amr'
			}, {
				xtype : 'filefield',
				id : 'form-file',
				emptyText : 'Select an image',
				fieldLabel : 'Photo',
				name : 'photo-path',
				buttonText : '',
				buttonConfig : {
					iconCls : 'upload-icon'
				}
			}],
			buttons : [{
				text : 'Enviar',
				handler : function() {
					var form = this.up('form').getForm();
					form.findField('fid').setValue(ultimoFeatureInserido.fid);
					// form.findField('user_name').setValue('amr');
					if (form.isValid()) {
						form.submit({
							url : 'file-upload.php',
							waitMsg : 'A carregar a imagem e a gerar miniaturas...',
							success : function(fp, o) {
								msg('Success', 'A imagem foi guardada no servidor com o nome "' + o.result.caminho);
								// this.up('form').getForm().reset();
								windowOcorrencia.hide();
								ultimoFeatureInserido = null;
							}
						});
					}
				}
			}, {
				text : 'Não enviar fotografia',
				handler : function() {
					// this.up('form').getForm().reset();
					windowOcorrencia.hide();
					ultimoFeatureInserido = null;
				}
			}]
		});

		var windowOcorrencia = Ext.create('Ext.Window', {
			title : 'Registo de uma ocorrência',
			width : 300,
			autoHeight : true,
			closable : false,
			modal : true,
			// height : 400,
			// x : 10,
			// y : 450,
			plain : true,
			headerPosition : 'top',
			// layout : 'fit',
			type : 'border',
			items : [formOcorrencia, formImagemOcorrencia]
		});

		mappanel = Ext.create('GeoExt.panel.Map', {
			title : 'A Minho Rua - Mapa das Ocorrências registadas',
			map : map,
			// center : '-8,40',
			center : '-48793,108614', // coordenadas
			// EPSG:3763
			zoom : 2, // 6,
			stateful : true,
			stateId : 'mappanel',
			// extent: '12.87,52.35,13.96,52.66',
			dockedItems : [{
				xtype : 'toolbar',
				dock : 'top',
				items : [
				/* {
				 text : 'Current center of the map',
				 handler : function() {
				 var c = GeoExt.panel.Map.guess().map.getCenter();
				 var z = GeoExt.panel.Map.guess().map.getZoom();
				 console.debug(z);
				 Ext.Msg.alert(this.getText(), c.toString() + ' zoom=' + z);
				 }
				 }, */
				{
					xtype : 'tbfill'
				}, {
					text : 'Ocorrências por estado',
					handler : function() {
						wfs_ocorrencia.styleMap.styles["default"] = style_estado;
						wfs_ocorrencia.redraw();
					}
				}, {
					text : 'Ocorrências por tipo',
					handler : function() {
						wfs_ocorrencia.styleMap.styles["default"] = style;
						wfs_ocorrencia.redraw();
					}
				}]
			}]
		});

		Ext.define('Ocorrencia', {
			extend : 'Ext.data.Model',
			fields : [{
				name : 'titulo',
			}, {
				name : 'participacao',
			}, {
				name : 'id_tipo_ocorrencia',
				type : 'int'
			}, {
				name : 'user_name',
			}, {
				name : 'id_estado',
				type : 'int'
			}, {
				name : 'data_hora',
				type : 'date'
				// dateFormat : 'timestamp'
			}],
			idProperty : 'id_ocorrencia'
		});

		// create the Data Store
		var store = Ext.create('Ext.data.Store', {
			id : 'store',
			model : 'Ocorrencia',
			// allow the grid to interact with the paging scroller by buffering
			buffered : true,

			// The topics-remote.php script appears to be hardcoded to use 50, and ignores this parameter, so we
			// are forced to use 50 here instead of a possibly more efficient value.
			pageSize : 50,

			// This web service seems slow, so keep lots of data in the pipeline ahead!
			leadingBufferZone : 1000,
			proxy : {
				// load using script tags for cross domain, if the data in on the same domain as
				// this page, an extjs store httpproxy would be better
				type : 'ajax',
				url : 'getOcorrenciasJson.php',
				reader : {
					root : 'ocorrencias',
					totalProperty : 'total'
				},
				// sends single sort as multi parameter
				simpleSortMode : true,

				// Parameter name to send filtering information in
				filterParam : 'query',

				// The PHP script just use query=<whatever>
				encodeFilters : function(filters) {
					return filters[0].value;
				}
			},
			listeners : {
				totalcountchange : onStoreSizeChange
			},
			remoteFilter : true,
			remoteSort : true,
			autoLoad : true
		});

		function onStoreSizeChange() {
			grid.down('#status').update({
				count : store.getTotalCount()
			});
		}

		function renderTopic(value, p, record) {
			return Ext.String.format('<a href="http://sencha.com/forum/showthread.php?p={1}" target="_blank">{0}</a>', value, record.getId());
		}

		var grid = Ext.create('Ext.grid.Panel', {
			width : 700,
			height : 500,
			// collapsible : true,
			title : 'Ocorrências',
			store : store,
			loadMask : true,
			dockedItems : [{
				dock : 'top',
				xtype : 'toolbar',
				items : [{
					width : 200,
					fieldLabel : 'Pesquisa',
					labelWidth : 50,
					xtype : 'searchfield',
					store : store
				}, '->', {
					xtype : 'component',
					itemId : 'status',
					tpl : 'encontradas: {count}',
					style : 'margin-right:5px'
				}]
			}],
			selModel : {
				pruneRemoved : false
			},
			multiSelect : true,
			viewConfig : {
				trackOver : false
			},
			// grid columns
			columns : [{
				xtype : 'rownumberer',
				width : 50,
				sortable : false
			}, {
				tdCls : 'x-grid-cell-topic',
				text : "Assunto",
				dataIndex : 'titulo',
				flex : 1,
				renderer : renderTopic,
				sortable : true
			}, {
				text : "Author",
				dataIndex : 'user_name',
				width : 100,
				hidden : false,
				sortable : true
			}, {
				text : "Participação",
				dataIndex : 'participacao',
				// align : 'center',
				width : 140,
				sortable : false
			}, {
				// id : 'last',
				text : "Registada em",
				dataIndex : 'data_hora',
				xtype : 'datecolumn',
				width : 130,
				renderer : Ext.util.Format.dateRenderer('Y-m-d'),
				sortable : true
			}]
		});

		var w = Ext.create('Ext.Window', {
			title : 'Ocorrências...',
			width : 400,
			height : 200,
			closable : false,
			x : 10,
			y : 450,
			plain : true,
			headerPosition : 'bottom',
			layout : 'fit',
			items : [grid]
		});
		w.show();
		w.alignTo(Ext.getBody(), "tr-tr", [-10, 10]);

		Ext.create('Ext.container.Viewport', {
			layout : 'fit',
			items : [mappanel],
			monitorResize : true,
			listeners : {
				resize : {
					fn : function(el) {
						// x, y
						w.alignTo(Ext.getBody(), "br-br", [-10, -80]);
						// console.log('panel resize');
					}
				}
			}
		});
	}
});
