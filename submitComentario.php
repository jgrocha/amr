<?php
require_once 'DB.php';
header('Content-type: application/json');

$servidor = $_SERVER['SERVER_NAME'];
$pos = strpos($servidor, 'localhost');
if ($pos === false) {
	$dsn = "pgsql://geobox:geobox2k13@amr.osgeopt.pt:5432/amr";
} else {
	$dsn = "pgsql://geobox:geobox@localhost:5432/ip";
}

$db = DB::connect($dsn, false);
if (DB::isError($db)) {
	$resposta["success"] = false;
	$resposta["errors"]["reason"] = $db -> getMessage();
	die(json_encode($resposta));
}
if (isset($_POST)) {
	// $_POST["fid"] = ocorrencias.1077
	$fidstr = $_POST["fid"];
	$partes = explode(".", $fidstr);
	$id_ocorrencia = $partes[1];
	$sql = "INSERT into amr.comentarios (id_ocorrencia, user_name, comentario, email, userid, resolvido) 
	values (" . $id_ocorrencia . ", '" . $_POST["user_name"] . "', '" . $_POST["comentario"] . "', '" . $_POST["email"] . "', '" . $_POST["userid"] . "', " . $_POST["resolvido"] . ")";
	$res = $db -> query($sql);
	if (DB::isError($res)) {
		$resposta["success"] = false;
		$resposta["errors"]["reason"] = $res -> getMessage();
		$resposta["errors"]["sql"] = $sql;
		die(json_encode($resposta));
	} else {
		$resposta["success"] = true;
		$resposta["errors"]["sql"] = $sql;
	}
} else {
	$resposta['success'] = 'false';
	$resposta["errors"]["reason"] = '_POST is not defined';
}
echo json_encode($resposta);
?>