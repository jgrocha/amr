Ext.require(['Ext.window.Window', 'Ext.tab.*', 'Ext.toolbar.Spacer', 'Ext.layout.container.Card', 'Ext.layout.container.Border']);

Ext.onReady(function() {
	var constrainedWin, constrainedWin2;

	Ext.onReady(function() {

		var task = {
			run : function() {
				Ext.MessageBox.updateText('????:' + Ext.util.Format.date(new Date(), 'Y-m-d g:1:s A'));
			},
			interval : 1000
		}

		Ext.TaskManager.start(task);

		Ext.util.Region.override({
			colors : ['red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet'],
			nextColor : 0,
			show : function() {
				var style = {
					display : 'block',
					position : 'absolute',
					top : this.top + 'px',
					left : this.left + 'px',
					height : ((this.bottom - this.top) + 1) + 'px',
					width : ((this.right - this.left) + 1) + 'px',
					opacity : 0.3,
					'pointer-events' : 'none',
					'z-index' : 9999999
				};
				if (!this.highlightEl) {
					style['background-color'] = this.colors[this.nextColor];
					Ext.util.Region.prototype.nextColor++;
					this.highlightEl = Ext.getBody().createChild({
						style : style
					});
					if (this.nextColor >= this.colors.length) {
						this.nextColor = 0;
					}
				} else {
					this.highlightEl.setStyle(style);
				}
			},
			hide : function() {
				if (this.highlightEl) {
					this.highlightEl.setStyle({
						display : 'none'
					});
				}
			}
		});

		var win2 = Ext.create('widget.window', {
			height : 200,
			width : 400,
			x : 450,
			y : 450,
			title : 'Constraining Window, plain: true',
			closable : false,
			plain : true,
			layout : 'fit',
			items : [ constrainedWin = Ext.create('Ext.Window', {
				title : 'Constrained Window',
				width : 200,
				height : 100,
				x : 20,
				y : 20,
				constrain : true,
				layout : 'fit',
				items : {
					border : false
				}
			}), constrainedWin2 = Ext.create('Ext.Window', {
				title : 'Header-Constrained Win',
				width : 200,
				height : 100,
				x : 120,
				y : 120,
				constrainHeader : true,
				layout : 'fit',
				items : {
					border : false
				}
			}), {
				border : false
			}]
		});
		win2.show();
		constrainedWin.show();
		constrainedWin2.show();

		Ext.create('Ext.Window', {
			title : 'Left Header, plain: true',
			width : 400,
			height : 200,
			x : 10,
			y : 200,
			plain : true,
			headerPosition : 'left',
			layout : 'fit',
			items : {
				border : false
			}
		}).show();

		Ext.create('Ext.Window', {
			// id: 'comboio',
			title : 'Right Header, plain: true',
			width : 400,
			height : 200,
			x : 450,
			y : 200,
			headerPosition : 'right',
			layout : 'fit',
			items : {
				border : false
			}
		}).show();

		var w = Ext.create('Ext.Window', {
			title : 'Bottom Header, plain: true',
			width : 400,
			height : 200,
			closable : false,
			x : 10,
			y : 450,
			plain : true,
			headerPosition : 'bottom',
			layout : 'fit',
			items : {
				border : false
			},
			monitorResize : true,
			listeners : {
				resize : {
					fn : function(el) {
						alert('panel resize');
					}
				}
			}
		});

		w.show();

		// w.alignTo(ancora, "tr-tr", [2,2]);
		w.alignTo(Ext.getBody(), "tr-tr", [-10, 10]);

		Ext.define('TipoOcorrencia', {
			extend : 'Ext.data.Model',
			fields : [{
				name : 'id',
				type : 'int'
			}, {
				name : 'ocorrencia',
				type : 'string'
			}]
		});

		var tipo_ocorrencia_store = [{
			"id" : 1,
			"ocorrencia" : "Acessos para Cidadãos com Mobilidade Reduzida"
		}, {
			"id" : 2,
			"ocorrencia" : "Animais Abandonados"
		}, {
			"id" : 3,
			"ocorrencia" : "Conservação da Iluminação Pública"
		}, {
			"id" : 4,
			"ocorrencia" : "Conservação das Ruas e Pavimento"
		}, {
			"id" : 5,
			"ocorrencia" : "Conservação de Parque Escolar"
		}, {
			"id" : 6,
			"ocorrencia" : "Estacionamento de Veículos"
		}, {
			"id" : 7,
			"ocorrencia" : "Limpeza de Valetas, Bermas e Caminhos"
		}, {
			"id" : 8,
			"ocorrencia" : "Limpeza e Conservação de Espaços Públicos"
		}, {
			"id" : 9,
			"ocorrencia" : "Manutenção de Ciclovias"
		}, {
			"id" : 10,
			"ocorrencia" : "Manutenção e Limpeza de Contentores e Ecopontos"
		}, {
			"id" : 11,
			"ocorrencia" : "Manutenção Rega e Limpeza de Jardins"
		}, {
			"id" : 12,
			"ocorrencia" : "Poluição Sonora"
		}, {
			"id" : 13,
			"ocorrencia" : "Publicidade, Outdoors e Cartazes"
		}, {
			"id" : 14,
			"ocorrencia" : "Recolha de Lixo"
		}, {
			"id" : 15,
			"ocorrencia" : "Rupturas de Águas ou Desvio de Tampas"
		}, {
			"id" : 16,
			"ocorrencia" : "Sinalização de Trânsito"
		}, {
			"id" : 17,
			"ocorrencia" : "Acessos para Cidadãos com Mobilidade Reduzida"
		}];

		var store = Ext.create('Ext.data.Store', {
			model : 'TipoOcorrencia',
			data : tipo_ocorrencia_store
		});

		Ext.create('Ext.panel.Panel', {
			layout : 'auto',
			title : 'A Minha Rua',
			width : '100%',
			renderTo : 'comboio',
			items : [{
				xtype : 'tabpanel',
				autoTabs : true,
				activeTab : 0,
				border : false,
				defaults : {
					autoHeight : true,
					bodyStyle : 'padding:10px'
				},
				items : [{
					title : 'Data Services',
					items : [{
						xtype : 'combo',
						queryMode : 'local',
						width : 280,
						emptyText : 'Escolha o tipo de ocorrência',
						store : store,
						displayField : 'ocorrencia',
						valueField : 'id'
					}]
				}]
			}]
		});
	});

});
